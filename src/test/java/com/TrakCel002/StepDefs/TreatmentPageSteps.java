package com.TrakCel002.StepDefs;

import com.TrakCel002.pages.DashboardPage;
import com.TrakCel002.pages.LoginPage;
import com.TrakCel002.pages.TreatmentsPage;
import com.TrakCel002.support.WorldHelper;
import com.TrakCel002.utilities.fileUtils.Props;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;




public class TreatmentPageSteps {

    private WorldHelper helper;
    private DashboardPage dashboardPage;
    private TreatmentsPage treatmentsPage;
    private LoginPage loginPage;

    private String admin1 = Props.getTData("admin1");
    private String siteSponsor = Props.getTData("siteSponsor");
    private String password1 = Props.getTData("password1");
    private String LeadClinic2 = Props.getTData("admin1");
    private String dashboardUser = Props.getTData("dashboard");
    private String basicUser = Props.getTData("basicClinic");
    private String basicClinic = Props.getTData("basicClinic");
    private String treatmentMgr = Props.getTData("admin1");
    private String treatmentCoord = Props.getTData("treatmentCoord");
    private String createTreatment = Props.getTData("createTreatment");
    private String leadClinic = Props.getTData("leadClinic");
    private String basicCMO = Props.getTData("basicCMO");
    private String leadCMO = Props.getTData("leadCMO");
    private String SuperUser = Props.getTData("SuperUser");
    private String SuperUser3 = Props.getTData("SuperUser3");
    private String SuperUser5 = Props.getTData("SuperUser5");
    private String allWorkFlows = Props.getTData("AllWorkFlows");


    public TreatmentPageSteps(WorldHelper helper) {
        this.helper = helper;
    }


    @Given("^I am on the treatments page$")
    public void iAmOnTheTreatmentsPage() {
              treatmentsPage =helper.getBasePage()
                .loadAppHome()
                .loginWith(admin1,password1)
                .clickOnTreatmentsNav()
                .addTreatmentWithCompletedPatientReg();
    }


    @Then("^Each treatment displays the required fields$")
    public void eachTreatmentDisplaysTheRequiredFields() {
        assertThat("Patient Name Element does not exist", treatmentsPage.patientNameElementExists(), is(true));
        assertThat("Date of birth Element does not exist", treatmentsPage.dateOfBirthExists(), is(true));
        assertThat("Last Birthday does not Exist", treatmentsPage.lastBirthdayExists(), is(true));
        assertThat("Product type element does not exist", treatmentsPage.productTypeElementExists(), is(true));
        assertThat("Manufacturing/Clinical site is not displayed", treatmentsPage.manufacturingSiteElementExists(), is(true));
    }

}


