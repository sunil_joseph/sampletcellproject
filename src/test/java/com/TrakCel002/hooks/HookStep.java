package com.TrakCel002.hooks;

import com.TrakCel002.browserUtils.WebDriverFactory;
import com.TrakCel002.utilities.fileUtils.Props;
import com.TrakCel002.utilities.fileUtils.Screenshots;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class HookStep {

    private String environment = System.getProperty("env").toLowerCase();

    @Before()
    public void beginTest() {
        WebDriverFactory.setDriver(Props.getValue("browser.name"));
    }

    @After()
    public void stopTest(Scenario scenario) {
        switch (environment) {
            case "aws":
            case "docker":
            case "local": {
                if (scenario.isFailed()) {
                    scenario.embed(Screenshots.takeShot(scenario.getName()), "image/png");
                }
                WebDriverFactory.closeBrowser();
            }
            break;
        }
    }

}

