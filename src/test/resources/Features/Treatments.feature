Feature: Treatments
  As a User I want to be able to carry out various treatment management functionality so that treatments
  are managed correctly

  @TC-113483 @wip
  Scenario: Treatment management
    Given I am on the treatments page
    Then Each treatment displays the required fields