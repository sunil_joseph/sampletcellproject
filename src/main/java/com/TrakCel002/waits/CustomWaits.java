package com.TrakCel002.waits;

import com.TrakCel002.WebControls.JavaScriptControl;
import com.TrakCel002.browserUtils.WebDriverFactory;
import com.TrakCel002.utilities.fileUtils.Props;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Collections;
import java.util.List;



/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */

public class CustomWaits {

    private CustomWaits() {
        throw new IllegalStateException("CustomWaits class");
    }

    protected static int time = Integer.parseInt(Props.getValue("global.time.out"));
    private static int maxTimeToWait = 60;
    private static WebDriver driver = WebDriverFactory.getDriver();
    private static Logger log = Logger.getLogger("TestLogger");
    private static String classAttr = "class";

    public static WebElement extComponentQuery(String componentQuery) {
        CustomWaits.checkPageReady();
        String cmd = "Ext.ComponentQuery.query('" + componentQuery + "')[0]";
        String js = "return " + cmd + ".id;";
        String id = (String) ((JavascriptExecutor) driver).executeScript(js);
        CustomWaits.waitForJavascript(10000, 3);
        try {
            return driver.findElement(By.id(id));
        } catch (org.openqa.selenium.JavascriptException e) {
            log.error(e.getMessage());
        }
        return driver.findElement(By.id(id));
    }

    public static void waitForNoOfSeconds(long seconds) {
        try {
            Thread.sleep(1000 * seconds);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    public static void waitForNoOfMilliSeconds(int milli) {
        try {
            Thread.sleep(milli);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    public static void checkPageReady() {
        waitForPageReadyState();
        waitForJavascript(15000, 500);
        waitForjQuery();
        log.info("Page Is loaded.");
    }

    /*
    waitForElement will fail if the element is not found
    isElementDisplayed will return false if element not found, but will not immediately fail
     */

    public static boolean isElementDisplayed(WebElement element) {
        CustomWaits.checkPageReady();
        try {
            return waitForPresenceOfElement(element) &&
                    waitForElementDisplayed(element) &&
                    waitForElementEnabled(element) &&
                    waitForExtEnabled(element);
        } catch (Exception e) {
            log.error(element.toString() + " was not found");
            return false;
        }
    }

    public static WebElement waitForElement(WebElement element) {
        CustomWaits.checkPageReady();
        waitForPresenceOfElement(element);
        waitForElementDisplayed(element);
        waitForElementEnabled(element);
        waitForExtEnabled(element);
        return element;
    }

    public static Boolean waitForTextInElement(WebElement element, String text) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        waitForElement(element);
        return wait.until(ExpectedConditions.textToBePresentInElement(element, text));
    }

    public static List<WebElement> waitForElements(WebElement element) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
            return wait.until(ExpectedConditions.visibilityOfAllElements(element));
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    public static void waitUntilClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitUntilTitleContains(String titleName) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        wait.until(ExpectedConditions.titleIs(titleName));
    }

    private static void waitForPageReadyState() {
        Wait<WebDriver> wait = new WebDriverWait(driver, maxTimeToWait);
        wait.until((ExpectedCondition<Boolean>) d ->
                String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState")).equals("complete"));
    }

    public static void waitForJavascript(int maxWaitMillis, int pollDelimiter) {
        double startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() < startTime + maxWaitMillis) {
            try {
                String prevState = driver.getPageSource();
                try {
                    Thread.sleep(pollDelimiter);
                } catch (InterruptedException e) {
                    log.debug(e);
                }
                if (prevState.equals(driver.getPageSource())) {
                    return;
                }
            } catch (TimeoutException te) {
                log.error("timeout exception in waitForJavascript");
                waitForNoOfSeconds(5);
            }
        }
    }

    public static void waitForjQuery() {
        int count = 0;
        if ((Boolean) JavaScriptControl.runJsScript("return window.jQuery != undefined")) {
            while (!(Boolean) JavaScriptControl.runJsScript("return jQuery.active == 0")) {
                waitForNoOfSeconds(1);
                if (count > 4)
                    break;
                count++;
            }
        }
    }

    public static boolean waitForClassChange(WebElement element, String attribute) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        return wait.until((ExpectedCondition<Boolean>) d -> {
            String classText = element.getAttribute(classAttr);
            log.info("element class is " + classText);
            return classText.contains(attribute);
        });
    }

    public static WebElement waitForElementLocated(By by) {
        int i = 0;
        while (driver.findElements(by).isEmpty() && i < 10) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.debug(e);
                Thread.currentThread().interrupt();
            }
            i++;
        }
        return driver.findElement(by);
    }

    public static boolean waitForAttribute(WebElement element, String attribute, String value) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        return wait.until((ExpectedCondition<Boolean>) d -> element.getAttribute(attribute).contains(value));
    }

    public static boolean waitForPresenceOfElement(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        wait.until(presenceOfElement(element));
        return true;
    }

    public static ExpectedCondition<WebElement> presenceOfElement(final WebElement element) {
        return new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver driver) {
                return element;
            }

            public String toString() {
                return "presence of element " + element;
            }
        };
    }

    public static boolean waitForElementDisplayed(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        return wait.until(elementIsDisplayed(element));
    }

    public static ExpectedCondition<Boolean> elementIsDisplayed(final WebElement element) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return element.isDisplayed();
            }
            public String toString() {
                return "display of element: " + element;
            }
        };
    }

    public static boolean waitForElementEnabled(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        return wait.until(elementIsEnabled(element));
    }

    public static ExpectedCondition<Boolean> elementIsEnabled(final WebElement element) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return element.isEnabled();
            }

            public String toString() {
                return "element to be enabled: " + element;
            }
        };
    }

    public static boolean waitForExtEnabled(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        return wait.until(elementIsExtEnabled(element));
    }

    public static ExpectedCondition<Boolean> elementIsExtEnabled(final WebElement element) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return !element.getAttribute(classAttr).contains("x-disabled");
            }

            public String toString() {
                return "element to be enabled: " + element;
            }
        };
    }

    public static ExpectedCondition<Boolean> absenceOfElementLocated(final By locator) {
        return driver -> {
            try {
                assert driver != null;
                driver.findElement(locator);
                return false;
            } catch (NoSuchElementException | StaleElementReferenceException e) {
                return true;
            }
        };
    }


    public static boolean waitForAbsenceOfElement(By by) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        try {
            wait.until(absenceOfElementLocated(by));
            return true;
        } catch (Exception e) {
            log.error("element was not present: " + by);
            return false;
        }
    }

    public static boolean waitForBySafe(By by) {
        return waitForPresenceOfBy(by) &&
                waitForByDisplayed(by) &&
                waitForByEnabled(by) &&
                waitForExtEnabledBy(by);
    }

    public static boolean waitForPresenceOfBy(By by) {
        CustomWaits.checkPageReady();
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
            return true;
        } catch (Exception e) {
            log.error("element was not present: " + driver.findElement(by));
            return false;
        }
    }

    public static boolean waitForByDisplayed(By by) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        try {
            wait.until((ExpectedCondition<Boolean>) d -> driver.findElement(by).isDisplayed());
            return true;
        } catch (Exception e) {
            log.error("element was not displayed: " + driver.findElement(by));
            return false;
        }
    }

    public static boolean waitForByEnabled(By by) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        try {
            wait.until((ExpectedCondition<Boolean>) d -> driver.findElement(by).isEnabled());
            return true;
        } catch (Exception e) {
            log.error("element was disabled: " + driver.findElement(by));
            return false;
        }
    }

    public static boolean waitForExtEnabledBy(By by) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        try {
            wait.until((ExpectedCondition<Boolean>) d -> (!driver.findElement(by).getAttribute("class").contains("x-disabled")));
            return true;
        } catch (Exception e) {
            log.error("x-disabled still present in element class: " + driver.findElement(by));
            return false;
        }
    }


    public static void waitForElementRemoval(WebElement element) {
        int i = 0;
        while (waitForPresenceOfElement(element) && i < 8) {
            waitForNoOfSeconds(1);
            i++;
        }
    }

    public static List<WebElement> waitForPresenceOfAllElements(List<WebElement> list) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        return wait.until(ExpectedConditions.visibilityOfAllElements(list));
    }

}

