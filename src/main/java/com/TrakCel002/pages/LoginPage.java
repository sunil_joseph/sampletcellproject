package com.TrakCel002.pages;

import com.TrakCel002.WebControls.ClickControl;
import com.TrakCel002.WebControls.SendControl;
import com.TrakCel002.utilities.fileUtils.Props;
import com.TrakCel002.waits.CustomWaits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPage extends BasePage {

    @FindBy(name = "username")
    private WebElement usernameElement;

    @FindBy(name = "password")
    private WebElement passwordElement;

    @FindBy(xpath = "//*[contains(text(),'[T]LOGIN')]")
    private WebElement localeLoginButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public <T> T loginAsUserToExpectPage(String username, Class<T> expectedLandingPage) {
        SendControl.enterText(usernameElement, username);
        SendControl.enterText(passwordElement, Props.getPassword(username.toLowerCase()));
        clickLoginButton();
        return PageFactory.initElements(webDriver, expectedLandingPage);
    }

    private void clickLoginButton() {
        WebElement loginElement = CustomWaits.extComponentQuery("button#loginButton");
        ClickControl.waitAndClick(loginElement);
    }

    private void clickLocaleLoginButton() {
        CustomWaits.checkPageReady();
        ClickControl.waitAndClick(CustomWaits.extComponentQuery("button[text=\"[T]LOGIN\"]"));
    }

    public DashboardPage loginWith(String admin1, String password1) {
        CustomWaits.checkPageReady();
        SendControl.enterText(usernameElement, admin1);
        SendControl.enterText(passwordElement, password1);
        clickLoginButton();
        return getDashboardPage();
    }

    public LoginPage loginWithNonAdmin(String user, String password1) {
        CustomWaits.checkPageReady();
        SendControl.enterText(usernameElement, user);
        SendControl.enterText(passwordElement, password1);
        clickLoginButton();
        return getLoginPage();
    }

    public DashboardPage loginAs(String username) {
        CustomWaits.checkPageReady();
        SendControl.enterText(usernameElement, username);
        SendControl.enterText(passwordElement, Props.getPassword(username.toLowerCase()));
        clickLoginButton();
        return getDashboardPage();
    }




    public void clickOK() {
        CustomWaits.checkPageReady();
        webDriver.switchTo().activeElement();
        WebElement okLoginFailed = CustomWaits.extComponentQuery("button#ok");
        ClickControl.waitAndClick(okLoginFailed);
    }


    public LoginPage selectLanguage(String language) {
        CustomWaits.checkPageReady();
        WebElement langDropDownElement = CustomWaits.extComponentQuery("combobox expandtrigger");
        ClickControl.waitAndClick(langDropDownElement);
        webDriver.switchTo().activeElement();
        WebElement ele = webDriver.findElement(By.id("ext-boundlist-1"));
        //wait.until(ExpectedConditions.visibilityOf(ele));
        ele.findElements(By.className("x-innerhtml"))
                .stream()
                .filter(element -> element.getText().equalsIgnoreCase(language))
                .findFirst()
                .get()
                .click();
        return this;
    }

}

