package com.TrakCel002.pages;

import com.TrakCel002.WebControls.ClickControl;
import com.TrakCel002.waits.CustomWaits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */

public class SelectRecordTypeModal extends BaseDynamicFormPage {

    @FindBy(xpath = "//div[contains(@class,'add-record') and contains(.,'Add Record')]")
    private WebElement addRecordButton;

    @FindBy(xpath = "//*[label/span[text()='Select Record Type']]")
    private WebElement openRecordTypeOptions;

    @FindBy(xpath = "//div[contains(text(),'CANCEL')]")
    private WebElement cancelButton;

    @FindBy(xpath = "//*[@id='addRecordProceedButton']")
    private WebElement addRecordProceedBtn;

    @FindBy(xpath = "//*[div/span[text()='Simple Shipment Booking']]")
    private WebElement simpleShipmentBooking;

    @FindBy(xpath = "//*[div/span[text()='Manufacturing Record']]")
    private WebElement manufacturingRecord;

    @FindBy(name = "cmofacilitykey")
    private WebElement selectCMOFacility;

//    @FindBy(xpath = "//*[text()='CMO: Test CMO 1 (Asia)']")
//    private WebElement selectFacilityInDropdown;

    @FindBy(xpath = "//*[contains(@class,'x-button') and .//div[contains(., 'CONFIRM')]]/button")
    private WebElement confirmBtn;

    @FindBy(xpath = "//*[div/span[text()='Label Printing Record']]")
    private WebElement labelPrintingRecord;

    @FindBy(xpath = "//*[div/span[text()='Shipment Booking Record']]")
    private WebElement shipmentBookingRecord;

    @FindBy(xpath = "//*[div/span[text()='Donor Registration Record']]")
    private WebElement donorRegistrationRecord;

    @FindBy(xpath = "//div[contains(text(),'Add Record to Patient Code')]")
    private WebElement patientCodeOnSelect;

    public SelectRecordTypeModal(WebDriver driver) {
        super(driver);
    }

    public boolean checkNoIdentifierTextReplaced() {
        CustomWaits.checkPageReady();
        webDriver.switchTo().activeElement();
        CustomWaits.isElementDisplayed(patientCodeOnSelect);
        return patientCodeOnSelect.isDisplayed();
    }

    public RecordsPage clickCancelButton() {
        WebElement cancelButtonCmp = CustomWaits.extComponentQuery("button[text=\"CANCEL\"]");
        ClickControl.waitAndClick(cancelButtonCmp);
        return getRecordsPage();
    }

    public boolean findRecordType(String record) {
        webDriver.switchTo().activeElement();
        CustomWaits.checkPageReady();
        List<WebElement> recordTypes = webDriver.findElements(By.xpath("//*//*[contains(@class," +
                "'x-list-inner-ct x-show-selection x-size-monitored x-paint-monitored x-no-row-lines x-layout-auto')]"));
        boolean recordExists = false;

        for (WebElement recordType : recordTypes) {
            if (recordType.getText().contains(record)) {
                recordExists = true;
                break;
            }
        }
        return recordExists;
    }
}
