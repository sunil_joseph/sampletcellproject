package com.TrakCel002.pages;


import com.TrakCel002.WebControls.ClickControl;
import com.TrakCel002.waits.CustomWaits;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */

public class DashboardPage extends BasePage {

    private static Logger log = Logger.getLogger("TestLogger");

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@placeholder='Search ID']")
    private WebElement searchBox;

    @FindBy(xpath = "//*[contains(@class,'cleartrigger')]")
    private WebElement clearFieldX;

    @FindBy(xpath = "//*[contains(@class,'x-selectfield')]//*[contains(@class,'x-trigger')]")
    private WebElement allProductElement;

    @FindBy(xpath = "//*[contains(@class,'view-filter-reset')]")
    private WebElement filterElement;

    @FindBy(xpath = "//div[contains(text(),'Patient Registration Record')]")
    private WebElement patientRegistrationWidget;

    @FindBy(xpath = "//div[contains(text(),'No identifier available')]")
    private WebElement treatmentInWidget;

    @FindBy(xpath = "//*[contains(@class,'main-toolbar__buttons__support')]")
    private WebElement needSupportIcon;

    private WebElement dashboardTile() {
        return CustomWaits.extComponentQuery("dashboardtracker panel[reference=\"dashboard-tracker-type-registration\"]");
    }

    private WebElement collectionsTile() {
        return CustomWaits.extComponentQuery("dashboardtracker panel[reference=\"dashboard-tracker-type-collection\"]");
    }

    private WebElement inboundShipmentsTile() {
        return CustomWaits.extComponentQuery("dashboardtracker panel[reference=\"dashboard-tracker-type-inbound\"]");
    }

    private WebElement manufacturingTile() {
        return CustomWaits.extComponentQuery("dashboardtracker panel[reference=\"dashboard-tracker-type-manufacturing\"]");
    }

    private WebElement outBoundShipmentsTile() {
        return CustomWaits.extComponentQuery("dashboardtracker panel[reference=\"dashboard-tracker-type-outbound\"]");
    }

    private WebElement infusionsTile() {
        return CustomWaits.extComponentQuery("dashboardtracker panel[reference=\"dashboard-tracker-type-infusion\"]");
    }

    private WebElement userToolBarButtonCmp() {
        return CustomWaits.extComponentQuery("button#adminButton");
    }


    private WebElement recentTreatmentWidget() {
        return CustomWaits.extComponentQuery("recenttreatment");
    }

    private WebElement recentRecordWidget() {
        return CustomWaits.extComponentQuery("recentrecord");
    }

    public boolean validateLogin() {
        CustomWaits.checkPageReady();
        return userToolBarButtonCmp().isDisplayed();
    }


    public boolean confirmDashboardTiles() {
        CustomWaits.checkPageReady();
        return dashboardTile().isDisplayed() &&
                collectionsTile().isDisplayed() &&
                inboundShipmentsTile().isDisplayed() &&
                manufacturingTile().isDisplayed() &&
                outBoundShipmentsTile().isDisplayed() &&
                infusionsTile().isDisplayed();
    }

    public boolean confirmTreatmentCompletedGraphs() {
        CustomWaits.checkPageReady();
        return webDriver.getPageSource().toLowerCase().contains("Treatments Completed (Last 6 Months)".toLowerCase());
    }

    public boolean confirmTreatmentsWidget() {
        CustomWaits.checkPageReady();
        return recentTreatmentWidget().isDisplayed();
    }

    public boolean confirmTreatmentTopFiveGraphs() {
        CustomWaits.checkPageReady();
        return webDriver.getPageSource().toLowerCase().contains("Treatments Completed (Top 5 Sites - Last 6 Months)".toLowerCase());
    }

    public boolean confirmRecordsWidget() {
        CustomWaits.checkPageReady();
        return recentRecordWidget().isDisplayed();
    }




    public boolean confirmSearchFieldText() {
        CustomWaits.checkPageReady();
        //wait.until(ExpectedConditions.elementToBeClickable(getSearchBox()));
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        String result = (String) js.executeScript("document.getElementsByName('Search ID')[0].value");
        return result == null || result.equals("");
    }

    public DashboardPage clickBackButton() {
        webDriver.navigate().back();
        CustomWaits.waitForNoOfSeconds(1);
        return this;
    }

    public DashboardPage clickForwardButton() {
        webDriver.navigate().forward();
        CustomWaits.waitForNoOfSeconds(1);
        return this;
    }

    public boolean assertPasswordChangeRequired() {
        CustomWaits.checkPageReady();
        return webDriver.getPageSource().toLowerCase().contains("New Password".toLowerCase());
    }

    public DashboardPage clickProductFilter() {
        CustomWaits.checkPageReady();
        ClickControl.waitAndClick(allProductElement);
        return this;
    }

    public DashboardPage selectProduct(String allProducts) {
        CustomWaits.checkPageReady();
        ClickControl.waitAndClick(webDriver.findElement(By.xpath("//*[text()='" + allProducts + "']")));
        return this;
    }

    public DashboardPage clickOnFilter() {
        CustomWaits.checkPageReady();
        ClickControl.waitAndClick(filterElement);
        CustomWaits.waitForAttribute(filterElement, "class", "x-hidden");
        return this;
    }

    public boolean confirmBackToAllProductDashboard() {
        CustomWaits.checkPageReady();
        return webDriver.getPageSource().toLowerCase().contains("Product".toLowerCase());
    }

    public RecordsPage clickOnRecordWidget() {
        CustomWaits.checkPageReady();
        ClickControl.waitAndClick(patientRegistrationWidget);
        return getRecordsPage();
    }

    public RecordsPage clickOnTreatmentInWidget() {
        CustomWaits.checkPageReady();
        ClickControl.waitAndClick(treatmentInWidget);
        return getRecordsPage();
    }

    public DashboardPage clickAllProducts() {
        CustomWaits.checkPageReady();
        ClickControl.waitAndClick(allProductElement);
        return getDashboardPage();
    }

    public boolean allProductDisplayed() {
        webDriver.switchTo().activeElement();
        return webDriver.getPageSource().contains("All Products");
    }

    public boolean areMenuElementsCorrect() {
        CustomWaits.checkPageReady();
        return getSearchBox().isDisplayed()
                && userToolBarButtonCmp().isDisplayed()
                && needSupportIcon.isDisplayed();
    }

}
