package com.TrakCel002.pages;


import com.TrakCel002.WebControls.ClickControl;
import com.TrakCel002.WebControls.DateFormatter;
import com.TrakCel002.waits.CustomWaits;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */

public class RecordsPage extends BasePage {

    private static final org.apache.log4j.Logger log = Logger.getLogger("TestLogger");

    @FindBy(xpath = "//div[contains(@class,'add-record')]")
    private WebElement addRecordButton;

    @FindBy(xpath = "//div[contains(text(),'Add Record to Patient Code')]")
    private WebElement addRecordDialogue;

    @FindBy(xpath = "//div[contains(text(),'Resume')]")
    private WebElement resumeButton;

    @FindBy(xpath = "//div[contains(@class,'pickerfield') and .//span[contains(text(),'Select Record Type')]]//input")
    private WebElement openRecordTypeOptions;

    @FindBy(xpath = "//*[@id='addRecordProceedButton']")
    private WebElement addRecordProceedBtn;

    @FindBy(xpath = "//b[contains(text(),'RECORDS LOCKED.')]")
    private WebElement recordLockedText;

    @FindBy(xpath = "//div[contains(text(),'Patient Registration Record')]")
    private WebElement patientRegististrationRecord;

    @FindBy(xpath = "//*[contains(@data-viewid,'ext-electronicrecordlist')]")
    private WebElement recordGrid;

    @FindBy(xpath = "//*[contains(@id,'treatmentlist')]")
    private WebElement treatmentsList;

    @FindBy(id = "recordActionMenuStop")
    private WebElement stopRecord;

    @FindBy(xpath = "//div[@class='x-icon-el x-font-icon mdi mdi-arrow-left']/parent::div")
    private WebElement clickToTreatments;

    @FindBy(xpath = "//*[contains(@data-viewid,'ext-electronicrecordlist') and .//*[contains(.,'Patient Registration Record') and .//*[contains(.,'In Progress')]]]")
    private WebElement patientRegistrationRecordElement;

    @FindBy(xpath = "//*[contains(@data-viewid,'ext-electronicrecordlist') and .//*[contains(.,'Donor Eligibility Record') and .//*[contains(.,'Signature')]]]")
    private WebElement donorEligibilityRecordWithSignature;

    @FindBy(xpath = "//*[contains(@data-componentid,'ext-gridrow') and .//*[contains(.,'Patient Registration Record') and .//*[contains(.,'In Progress')]]]")
    private WebElement inProgressPatientRegistrationRecord;

    @FindBy(xpath = "//*[contains(@data-viewid,'ext-electronicrecordlist') and .//*[contains(.,'Donor Eligibility Record') and .//*[contains(.,'In Progress')]]]")
    private WebElement donorEligibilityRecord;

    @FindBy(xpath = "//div[contains(text(),'STOP RECORD')]")
    private WebElement confirmStopRecordBtn;

    @FindBy(xpath = "//div[contains(text(),'No records are available to start')]")
    private WebElement recordListWarningBanner;

    @FindBy(xpath = "//div[contains(text(),'No records are available to start')]")
    private WebElement noRecordToStartElement;

    public RecordsPage(WebDriver driver) {
        super(driver);
    }

    public <T> T addRecord(String recordType, Class<T> expectedPage) {
        addRecord(recordType);
        return PageFactory.initElements(webDriver, expectedPage);
    }

    public <T> T gotToRecord(String recordType, Class<T> expectedPage) {
        goToRecord(recordType);
        return PageFactory.initElements(webDriver, expectedPage);
    }

    private void clickAddRecordButtonComponent() {
        CustomWaits.checkPageReady();
        extUtils.buttonWithTextLike("Add Record").click();
    }

    public void addRecord(String recordType) {
        CustomWaits.checkPageReady();
        clickAddRecordButtonAndOpenRecordDropDown();
        String xpath = "//span[text()='" + recordType + "']";
        ClickControl.waitAndClick(By.xpath(xpath));
        webDriver.switchTo().activeElement();
        ClickControl.waitAndClick(addRecordProceedBtn);
        CustomWaits.checkPageReady();
    }

    public SelectRecordTypeModal clickAddRecordButtonAndOpenRecordDropDown() {
        CustomWaits.checkPageReady();
        clickAddRecordButtonComponent();
        ClickControl.waitAndClick(openRecordTypeOptions);
        return getSelectRecordTypePage();
    }
    public ManufacturingRecordDynamicFormPage addManufacturingRecord() {
        return addRecord("Manufacturing Record", ManufacturingRecordDynamicFormPage.class);
    }

    public boolean addRecordButtonVisible() {
        CustomWaits.checkPageReady();
        WebElement addRecordBtn = CustomWaits.extComponentQuery("button#addRecordButton");
        return CustomWaits.isElementDisplayed(addRecordBtn);
    }

    public boolean confirmRecordLockedBanner() {
        CustomWaits.checkPageReady();
        CustomWaits.isElementDisplayed(recordLockedText);
        return webDriver.findElement(By.xpath("//b[contains(text(),'RECORDS LOCKED.')]")).isDisplayed();
    }

    public RecordsPage verifyDateFormat(String createdText) {
        String subStringDateTime = createdText.substring(0, 17);
        String subStringTimezone = createdText.substring(18, 23);
        assertTrue(DateFormatter.isValidFormat("dd MMM uuuu HH:mm", subStringDateTime));
        assert subStringTimezone.equals("+0100") || subStringTimezone.equals("+0000");
        return this;
    }



    //ToDo: Come back and fix this to get it to work with extUitils
    public void goToRecord(String recordType) {
        boolean staleElement = true;
        webDriver.navigate().refresh();
        CustomWaits.checkPageReady();
        while (staleElement) {
            try {
                CustomWaits.checkPageReady();
                WebElement record = webDriver.findElement(By.xpath("//div[contains(@class,'t-monitored x-listitem-grid-card-view x-gridrow-grid-card-view') and .//div[contains(.,'" + recordType + "')]]"));
                ClickControl.waitAndClick(record);
                staleElement = false;
            } catch (StaleElementReferenceException e) {
                staleElement = true;
            }
        }
    }

    public ContinueTreatmentRecordDetailPage goToContinueTreatmentRecord() {
        webDriver.navigate().refresh();
        this.goToRecord("Continue Treatment Record ");
        return PageFactory.initElements(webDriver, ContinueTreatmentRecordDetailPage.class);
    }

    public boolean confirmBackToRecords() {
        CustomWaits.checkPageReady();
        return extUtils.recordCard("Patient Registration Record").isDisplayed();
    }



    public boolean atRecordsPage() {
        CustomWaits.checkPageReady();
        return webDriver.getCurrentUrl().contains("records".toLowerCase());
    }

    public RecordsPage stopSpecificRecord(String recordType) {
        CustomWaits.checkPageReady();
        webDriver.navigate().refresh();
        String xpath = "//*[contains(@class,'x-listitem" +
                " x-gridrow x-component x-paint-monitored') " +
                "and contains(.,'" + recordType + "')]" +
                "//div[contains(@class,'view__action-button')]";
        ClickControl.waitAndClick((By.xpath(xpath)));
        ClickControl.waitAndClick(stopRecord);
        ClickControl.waitAndClick(confirmStopRecordBtn);
        return getRecordsPage();
    }

    public boolean recordHasStatusOf(String recordType, String status) { //Case sensitive
        String xpath = "//*[contains(@class,'x-listitem x-gridrow" +
                " x-component x-paint-monitored')and contains" +
                "(.,'" + recordType + "')]//div[contains(text(),'" + status + "')]";
        return CustomWaits.waitForBySafe(By.xpath(xpath));
    }



    public boolean checkForModifiedTextOnCard(String recordName) {
        CustomWaits.checkPageReady();
        String userNameForLastMod = webDriver.findElement(By.xpath("//div[contains(@data-viewid,'recordlist') and contains(.,'" + recordName + "')]//div[contains(@id,'erdetail')]")).getText();
        log.info(userNameForLastMod);
        return userNameForLastMod.contains("Modified");
    }

    public boolean checkForRecordIcon(String records) {
        CustomWaits.checkPageReady();
        WebElement recordIcon = webDriver.findElement(By.xpath("//div[contains(@data-viewid,'recordlist') and contains(.,'" + records + "')]//div[@class='grid-card-view__record-card__icon']"));
        return CustomWaits.isElementDisplayed(recordIcon);
    }

    public boolean noRecordsToStartMessageVisible() {
        CustomWaits.checkPageReady();
        return noRecordToStartElement.isDisplayed();
    }

}

