package com.TrakCel002.pages;

import com.TrakCel002.WebControls.ClickControl;
import com.TrakCel002.WebControls.JavaScriptControl;
import com.TrakCel002.WebControls.SendControl;
import com.TrakCel002.utilities.fileUtils.FileDownload;
import com.TrakCel002.waits.CustomWaits;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */


public class PatientTestResultsRecordDetailPage extends BaseRecordDetailPage {

    @FindBy(xpath = "//a[contains(text(),'Trakcel-labels.pdf')]")
    private WebElement labelsPDFDownloadElement;

    @FindBy(xpath = "//div[contains(@data-viewid,'signature') and .//*[contains(.,'Review')]]//div[contains(@class,'x-button') and .//*[contains(.,'Sign')]]")
    private WebElement openESignReviewElement;

    @FindBy(xpath = "//*[contains(@class,'textfield') and .//*[contains(.,'Username')]]//input")
    public WebElement enterESignUsername;

    @FindBy(xpath = "//*[contains(@class,'textfield') and .//*[contains(.,'Password')]]//input")
    public WebElement enterESignPassword;

    @FindBy(xpath = "//*[contains(@class,'x-button') and .//div[contains(., 'Sign')]]/button")
    public WebElement SIGN;

    @FindBy(xpath = "//span[text()='Patient']")
    private WebElement patientElement;

    @FindBy(xpath = "//div[@class='subrecord-task-form-value'][contains(text(),'Patient')]")
    private WebElement patientField;


    public PatientTestResultsRecordDetailPage(WebDriver driver) {
        super(driver);
    }

    public boolean checkRecordForFile() {
        CustomWaits.checkPageReady();
        return webDriver.getPageSource().contains("Trakcel-labels.pdf");
    }

    public boolean downloadFileFromRecord() {
        CustomWaits.checkPageReady();
        ClickControl.waitAndClick(labelsPDFDownloadElement);
        CustomWaits.checkPageReady();
        return FileDownload.isFileDownloaded("Trakcel-labels.pdf");
    }

    public PatientTestResultsRecordDetailPage signPatientTestResultsRecord() {
        CustomWaits.checkPageReady();
        CustomWaits.checkPageReady();
        this.clickESignReview();
        this.eSignReview();
        return getPatientTestResultsRecordDetailPage();
    }

    private void clickESignReview() {
        CustomWaits.checkPageReady();
        ClickControl.waitAndClick(openESignReviewElement);
        webDriver.switchTo().activeElement();
    }

    private void eSignReview() {
        CustomWaits.checkPageReady();
        SendControl.enterText(enterESignUsername, "autoadmin");
        SendControl.enterText(enterESignPassword, "qweqwe123A");
        String query = "var sign = document.evaluate(\"//*[contains(@class,'x-button') and .//div[contains(., 'SIGN')]]/button\", " +
                "document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;\n" +
                "sign.click();";
        JavaScriptControl.runJsScript(query);
        CustomWaits.waitForJavascript(2000, 3);
    }

    public boolean correctField() {
        CustomWaits.checkPageReady();
        return patientField.isDisplayed();
    }
}
