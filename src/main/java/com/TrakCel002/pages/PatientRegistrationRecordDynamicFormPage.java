package com.TrakCel002.pages;

import com.TrakCel002.WebControls.*;
import com.TrakCel002.utilities.fileUtils.DIFactory;
import com.TrakCel002.waits.CustomWaits;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDate;

public class PatientRegistrationRecordDynamicFormPage  extends BaseDynamicFormPage{



    @FindBy(name = "PATIENT_CODE")
    private WebElement patientCodeElement;

    @FindBy(name = "name")
    private WebElement patientNameElement;

    @FindBy(name = "mandatory")
    private WebElement mandatory;

    @FindBy(name = "comments")
    private WebElement commentsElement;

    @FindBy(xpath = "//*[contains(@class,'confirm-button') ]//*[contains(text(),'CONFIRM')]")
    private WebElement confirmButtonElement;

    @FindBy(xpath = "//*[contains(@class,'x-button') and .//div[contains(.,'SAVE')]]")
    private WebElement saveBtn;

    @FindBy(xpath = "//*[text()='REPORTS']")
    private WebElement reportsMenu;

    @FindBy(xpath = "//span[contains(text(),'Optional')]")
    private WebElement optionalCheckboxElement;

    @FindBy(xpath = "//span[contains(text(),'Mandatory check box with a large field label which')]")
    private WebElement mandatoryCheckboxElement;

    @FindBy(name = "altcontact")
    private WebElement alternativeContact;

    @FindBy(name = "past_appointment")
    private WebElement mandatoryDateElement;

    @FindBy(xpath = "//div[contains(text(),'You need to tick this box!')]")
    private WebElement validationMessage;

    @FindBy(name = "dob")
    private WebElement dobElement;

    @FindBy(name = "physicianusername")
    private WebElement physicianUserElement;

    @FindBy(xpath = "//span[text()='AutoLogin']")
    private WebElement autoLoginUser;

    private static Logger log = Logger.getLogger("TestLogger");

    public PatientRegistrationRecordDynamicFormPage(WebDriver driver) {
        super(driver);
    }

    public PatientRegistrationRecordDynamicFormPage completePatientRegistration() {
        enterPatientCode();
        enterPatientName();
        enterDOB();
        enterMandatoryDate();
        tickMandatoryCheckbox();
        clickProceed();
        return getPatientRegistrationRecordDynamicFormPage();
    }
    public TreatmentsPage confirmPatientRegistration() {
        CustomWaits.checkPageReady();
        clickProceed();
        CustomWaits.waitForNoOfSeconds(1);
        return getTreatmentsPage();
    }
    private void tickMandatoryCheckbox() {
        CustomWaits.checkPageReady();
        JavaScriptControl.runJsScript("document.getElementsByName('mandatory')[0].click();");
    }

    private void generatePatientCodeTwo() {
        CustomWaits.checkPageReady();
        TextFieldsControl.inputText(patientCodeElement, DIFactory.patientCodeSecond());
    }

    private void enterPatientNameTwo() {
        CustomWaits.checkPageReady();
        TextFieldsControl.inputText(patientNameElement, DIFactory.generatePatientNameSecond());
    }

    private void enterPatientCode() {
        CustomWaits.checkPageReady();
        TextFieldsControl.inputText(patientCodeElement, DIFactory.generatePatientCode());
    }

    private void enterPatientCodeFirstPI() {
        CustomWaits.checkPageReady();
        TextFieldsControl.inputText(patientCodeElement, DIFactory.generatePatientCodeOldest());
    }

    private void enterPatientName() {
        CustomWaits.checkPageReady();
        TextFieldsControl.inputText(patientNameElement, DIFactory.generatePatientName());
    }

    private void enterMandatoryDate() {
        CustomWaits.checkPageReady();
        JavaScriptControl.scrollElementIntoView(mandatoryDateElement);
        CustomWaits.isElementDisplayed(mandatoryDateElement);
        CustomWaits.waitForNoOfMilliSeconds(100);
        ClickControl.waitAndClick(mandatoryDateElement);
        CustomWaits.checkPageReady();
        DatePicker.datePicker(webDriver, -1);
    }




    private void enterInvalidPatientNameDetails() {
        CustomWaits.checkPageReady();
        CustomWaits.waitUntilClickable(patientNameElement);
        TextFieldsControl.inputText(patientNameElement, DIFactory.generateInvalidPatientName());
        patientNameElement.sendKeys(Keys.TAB);
    }

    public PatientRegistrationRecordDynamicFormPage enterInvalidPatientName() {
        this.enterPatientCode();
        this.enterInvalidPatientNameDetails();
        return getPatientRegistrationRecordDynamicFormPage();
    }

    public void invalidPatientID() {
        CustomWaits.checkPageReady();
        CustomWaits.waitUntilClickable(patientCodeElement);
        TextFieldsControl.inputText(patientCodeElement, "123456");
    }

    public PatientRegistrationRecordDynamicFormPage enterInvalidPatientID() {
        this.invalidPatientID();
        return getPatientRegistrationRecordDynamicFormPage();
    }

    public boolean checkSaveButton() {
        CustomWaits.checkPageReady();
        if (!CustomWaits.isElementDisplayed(saveBtn)) {
            return true;
        } else return CustomWaits.isElementDisplayed(saveBtn) && (!saveBtn.isEnabled());
        //ToDo:This needs to be refactored
    }

    public boolean proceedButtonStatus() {
        CustomWaits.checkPageReady();
        return webDriver.findElement(By.xpath("//div[text()='PROCEED']")).getAttribute("class").contains("x-disabled");
    }


    public boolean verifyGeneratedFieldLabel(String patientName, String patientCode) {
        String expectedLabel = "CLINICAL_SITE_1-" + patientName + "-" + patientCode + "-" + LocalDate.now().getYear();
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        return (js.executeScript("return $(\"[name=generatedlabel]\")[0].value").equals(expectedLabel));
    }

    public TreatmentsPage fillPatientRegFormWithMandatoryAndProceed() {
        enterPatientCode();
        enterPatientName();
        enterDOB();
        enterMandatoryDate();
        tickMandatoryCheckbox();
        clickProceedAndConfirm();
        return getTreatmentsPage();
    }

    private void clickOnProceed() {
        CustomWaits.checkPageReady();
        String query = "var proceed = document.evaluate(\"//*[contains(@class,'x-button') and .//div[contains(., 'PROCEED')]]/button\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;\n" +
                "proceed.click();";
        JavaScriptControl.runJsScript(query);
        CustomWaits.checkPageReady();
    }

    private void clickProceedAndConfirm() {
        CustomWaits.checkPageReady();
        clickProceed();
        clickProceed();
        CustomWaits.checkPageReady();
    }

    public boolean verifyAltContactPopulation(String patientName) {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        return (js.executeScript("return $(\"[name=altcontact]\")[0].value").equals(patientName));
    }


    private void enterAlternativeContactDetails(String altContact) {
        CustomWaits.checkPageReady();
        CustomWaits.waitUntilClickable(alternativeContact);
        alternativeContact.clear();
        SendControl.enterText(alternativeContact, altContact);
    }




    public boolean confirmNoDataHasBeenRetained() {
        CustomWaits.checkPageReady();
        WebElement patientCodeInputBox = webDriver.findElement(By.name("PATIENT_CODE"));
        String textInsidePatientInputBox = patientCodeInputBox.getAttribute("name");
        if (textInsidePatientInputBox.isEmpty()) {
            log.info("Input field is empty");
        }
        return true;
    }



    private void enterDOB() {
        CustomWaits.checkPageReady();
        JavaScriptControl.scrollElementIntoView(dobElement);
        CustomWaits.isElementDisplayed(dobElement);
        CustomWaits.waitForNoOfMilliSeconds(100);
        ClickControl.waitAndClick(dobElement);
        CustomWaits.checkPageReady();
        DatePicker.datePicker(webDriver, -3);
    }


    public String getPatientCodeValue() {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        return (js.executeScript("return $(\"[name=PATIENT_CODE]\")[0].value")).toString();
    }

    public String getPatientNameValue() {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        return (js.executeScript("return $(\"[name=name]\")[0].value")).toString();
    }

}

