package com.TrakCel002.pages;


import com.TrakCel002.WebControls.ClickControl;
import com.TrakCel002.WebControls.DropDownControl;
import com.TrakCel002.waits.CustomWaits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ProductModalPage extends BaseDynamicFormPage {

    @FindBy(xpath = "//*[contains(@data-componentid,'ext-combo') and .//*[contains(.,'Select')]]//input")
    private WebElement productOptions;

    @FindBy(xpath = "//*[@id='confirmNewTreatmentButton']")
    private WebElement confirmNewTreatment;

    public ProductModalPage(WebDriver driver) {
        super(driver);
    }

    public PatientRegistrationRecordDynamicFormPage selectProduct(String productName) {
        dialogExpandTrigger();
        DropDownControl.selectTwaDropDown(webDriver, productName);
        clickCreateTreatmentButton();
        return getPatientRegistrationRecordDynamicFormPage();
    }

    private void clickCreateTreatmentButton() {
        CustomWaits.checkPageReady();
        WebElement createNewTreatmentButton = CustomWaits.extComponentQuery("button[text=\"CREATE TREATMENT\"]");
        ClickControl.waitAndClick(createNewTreatmentButton);
    }

    private void dialogExpandTrigger() {
        CustomWaits.checkPageReady();
        WebElement dialogExpandTrigger = CustomWaits.extComponentQuery("dialog expandtrigger");
        ClickControl.waitAndClick(dialogExpandTrigger);
    }

    public ProductModalPage doNotSelectProduct() {
        CustomWaits.isElementDisplayed(productOptions);
        return getProductPage();
    }

    public boolean confirmButtonDisabled() {
        CustomWaits.isElementDisplayed(confirmNewTreatment);
        return webDriver.findElement(By.id("confirmNewTreatmentButton")).getAttribute("class").contains("disabled");
    }

}

