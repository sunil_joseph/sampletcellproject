package com.TrakCel002.pages;

import com.TrakCel002.Ext.ExtUtils;
import com.TrakCel002.WebControls.ClickControl;
import com.TrakCel002.support.WorldHelper;
import com.TrakCel002.utilities.fileUtils.DIFactory;
import com.TrakCel002.utilities.fileUtils.Props;
import com.TrakCel002.waits.CustomWaits;
import com.google.common.collect.ImmutableList;
import cucumber.api.DataTable;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;
import java.util.stream.Collectors;

public class BasePage extends WorldHelper {

    public BasePage(WebDriver driver) {
        this.webDriver = driver;
    }

    private String mailCatcher = Props.getValue("mailCatcher");
    static String currentEnv = System.getProperty("env");
    public static final Logger log = Logger.getLogger("TestLogger");
    public final ExtUtils extUtils = new ExtUtils();
   public WebDriverWait wait = new WebDriverWait(webDriver, 60);
    public static String getCurrentEnv() {
        return currentEnv;
    }

    public String getTreatmentURL() {
        return DIFactory.getTreatmentURL();
    }

    @FindBy(xpath = "//div[contains(text(),'SCHEDULING')]")
    private WebElement schedulingSidebar;

    @FindBy(xpath = "//div[contains(@class,' x-pack-start x-scroller')]//div[contains(@class,'x-component x-button view-header__back')]//div[contains(text(),'Back to Treatment')]/parent::div")
    private WebElement backToTreatmentButton;

    @FindBy(xpath = "//input[@placeholder='Search ID']")
    private WebElement searchBox;

    @FindBy(xpath = "//div[@class='x-expandtrigger x-trigger x-interactive x-expandtrigger-multifilter x-trigger-multifilter']")
    private WebElement statusFilter;

    @FindBy(xpath = "//*[contains(@data-viewid,'ext-boundlist') and .//span[text()='All Products']]")
    private WebElement productFilterElement;

    @FindBy(xpath = "//*[contains(@data-viewid,'ext-boundlist') and .//span[contains(.,'TWA Product One')]]")
    private WebElement productOneElement;

    @FindBy(xpath = "//*[contains(@data-viewid,'ext-boundlist') and .//span[contains(.,'TWA Product Two')]]")
    private WebElement productTwoElement;

    public WebElement getSearchBox() {
        return CustomWaits.extComponentQuery("container[reference=\"toolbarSearch\"]");
    }

    public void clickUserToolBar() {
        CustomWaits.checkPageReady();
        WebElement adminButton = CustomWaits.extComponentQuery("button#adminButton");
        if (adminButton.isDisplayed()) {
            ClickControl.waitAndClick(adminButton);
            CustomWaits.checkPageReady();
        } else {
            scrollToTopMenuBar();
            ClickControl.waitAndClick(adminButton);
        }
        webDriver.switchTo().activeElement();
    }

    public WebElement getAdminMenuOptions(String menuOption) {
        CustomWaits.checkPageReady();
        clickUserToolBar();
        webDriver.switchTo().activeElement();
        return webDriver.findElement(By.xpath("//*[text()='" + menuOption + "']"));
    }

    private void clickLogoutBtn() {
        WebElement logoutBtnCmp = CustomWaits.extComponentQuery("menuitem#logOutBtn");
        ClickControl.waitAndClick(logoutBtnCmp);
    }

    private void clickYesLogoutBtn() {
        WebElement yesBtn = CustomWaits.extComponentQuery("button[text=\"YES\"]");
        ClickControl.waitAndClick(yesBtn);
    }

    public LoginPage loadAppHome() {
               webDriver.navigate().to(Props.getValue("base.url"));
        CustomWaits.checkPageReady();
        return getLoginPage();
    }

    public LoginPage logout() {
        clickUserToolBar();
        clickLogoutBtn();
        clickYesLogoutBtn();
        return getLoginPage();
    }

    public LoginPage returnToTopThenLogout() {
        CustomWaits.checkPageReady();
        scrollToTopMenuBar();
        logout();
        return getLoginPage();
    }

    public TreatmentsPage clickOnTreatmentsNav() {
        CustomWaits.checkPageReady();
        WebElement treatmentNav = CustomWaits.extComponentQuery("menuitem#treatmentNav");
        if (treatmentNav.isDisplayed()) {
            ClickControl.waitAndClick(treatmentNav);
            CustomWaits.checkPageReady();
        } else
            scrollToTopMenuBar().clickOnTreatmentsNav();
        CustomWaits.checkPageReady();
        return getTreatmentsPage();
    }

        private void openNewTabToMailCatcher() {
        ((JavascriptExecutor) webDriver).executeScript("window.open('" + mailCatcher + "','_blank');");
    }

    private void switchToNewTab() {
        String subWindowHandler = null;
        Set<String> handles = webDriver.getWindowHandles();
        Iterator<String> iterator = handles.iterator();
        while (iterator.hasNext()) {
            subWindowHandler = iterator.next();
        }
        webDriver.switchTo().window(subWindowHandler);
    }

    public void switchToBackToParentTab() {
        ArrayList<String> tabs = new ArrayList<>(webDriver.getWindowHandles());
        webDriver.switchTo().window(tabs.get(0));
    }

    public String switchURL() {
        String env = getCurrentEnv();
        String currentURL = null;
        switch (env) {
            case "docker":
                currentURL = "http://docker.dev.trakcel.com:8899";
                break;
            case "local":
                currentURL = "http://localhost:8080";
                break;
            case "aws":
            case "sauce":
                currentURL = "https://uitest.trakceldev.com";
                break;
            default:
                return null;
        }
        return currentURL;
    }

    public boolean confirmUrl(String url) {
        CustomWaits.checkPageReady();
        log.info(url);
        return webDriver.getCurrentUrl().toLowerCase().contains(url.toLowerCase());
    }

    public String getPageTitle() {
        CustomWaits.checkPageReady();
        return webDriver.getTitle();
    }

    public BasePage scrollToTopMenuBar() {
        WebElement mdiarrowbtn = CustomWaits.extComponentQuery("button[iconCls=\"mdi mdi-arrow-up\"]");
        ClickControl.waitAndClick(mdiarrowbtn);
        return getBasePage();
    }

    public DataTable buildTabs(String... tabs) {
        List<List<String>> rows = Arrays.stream(tabs)
                .map(ImmutableList::of)
                .collect(Collectors.toList());
        return DataTable.create(rows);
    }
}

