package com.TrakCel002.pages;

import com.TrakCel002.WebControls.ClickControl;
import com.TrakCel002.WebControls.DateFormatter;
import com.TrakCel002.WebControls.JavaScriptControl;
import com.TrakCel002.waits.CustomWaits;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */

public class BaseRecordDetailPage extends BasePage {

    private static final Logger log = Logger.getLogger("TestLogger");

    @FindBys({@FindBy(xpath = "//div[contains(@id,'subrecorddetailspanel')]//div[contains(@class,'label-body')]//div[@class='x-innerhtml']")})
    private List<WebElement> subRecords;

    @FindBy(xpath = "//div[contains(@id,'recorddetail')]//div[contains(@class,'view-header-title')]/div[2]")
    private WebElement recordName;

    @FindBy(xpath = "//div[contains(@id,'recorddetail')]//div[contains(@class,'view-header-title--minor')]")
    private WebElement recordVersion;

    @FindBy(xpath = "//*[contains(@class,'locked-toggle')]")
    private WebElement lockStatus;

    @FindBy(xpath = "//*[contains(@class,'locked-toggle')]//*[contains(@id,'slider')]")
    private WebElement lockToggle;

    @FindBy(xpath = "//div[contains(@class,'x-button') and contains(.,'CONFIRM')]")
    private WebElement lockConfirm;

    @FindBy(xpath = "//div[contains(@data-componentid,'electronicrecordsummary')]//div[contains(@class,'status-indicator')]//div[contains(@class,'innerhtml')]")
    private WebElement recordStatus;

    @FindBys({@FindBy(xpath = "//div[contains(@id,'electronicrecordversiondetail')]")})
    private List<WebElement> recordVersions;

    @FindBy(xpath = "//div[contains(@id,'outstandingtasklist')]//div[contains(@data-viewid,'tasklist')]//div[contains(@data-componentid,'taskname')]")
    private WebElement outstandingTaskName;

    @FindBy(xpath = "//div[contains(@class,'x-listitem-grid-list-view ')]//div[contains(@class,'x-button')]")
    private WebElement startTaskBtn;

    @FindBy(xpath = "//div[contains(@class,'textfield') and contains(.,'Last Modified:')]//input")
    private WebElement lastModifiedBy;

    @FindBy(xpath = "//*[contains(@id,'ext-electronicrecordversionlist')]")
    private WebElement versionGrid;

    @FindBys({
            @FindBy(xpath = "//div[contains(@data-viewid,'audit')]")})
    private List<WebElement> auditLogEvents;

    @FindBy(xpath = "//div[contains(text(),'This record is out of date. A new version exists.')]")
    private WebElement messageWarningBanner;

    @FindBy(xpath = "//b[contains(text(),'WARNING...')]")
    private WebElement warningMessage;

    @FindBy(xpath = "//div[@class='x-component x-button subrecord-rerun-button x-ignore-theme x-has-icon x-icon-align-left x-arrow-align-right x-paint-monitored x-layout-box-item x-layout-hbox-item']")
    private WebElement editButton;

    public BaseRecordDetailPage(WebDriver driver) {
        super(driver);
    }

    public void clickResume(){
        CustomWaits.checkPageReady();
        extUtils.buttonWithTextLike("Resume").click();
    }

    public String getRecordName() {
        return CustomWaits.waitForElement(recordName).getText();
    }

    public String getOutstandingTaskName() {
        CustomWaits.isElementDisplayed(outstandingTaskName);
        return outstandingTaskName.getText();
    }

    public void startOutstandingTask() {
        CustomWaits.checkPageReady();
        extUtils.buttonWithTextLike("Start").click();
    }

    public boolean isRecordLocked() {
        CustomWaits.isElementDisplayed(lockStatus);
        return (lockStatus.getText().contains("Locked"));
    }

    public boolean isRecordPermalocked() {
        return CustomWaits.waitForClassChange(lockStatus, "x-hidden");
    }


    public String getRecordStatus() {
        CustomWaits.isElementDisplayed(recordStatus);
        return recordStatus.getText();
    }

    public String getRecordVersion() {
        CustomWaits.isElementDisplayed(recordVersion);
        return recordVersion.getText().replace("(Version ", "").replace(")", "");
    }

    public List<String> getSubrecordNames() {
        CustomWaits.checkPageReady();
        CustomWaits.isElementDisplayed(subRecords.get(0));
        return subRecords.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public String getLastModifiedBy() {
        return JavaScriptControl.jsGetValue(lastModifiedBy);
    }

    public void expandAuditLog() {
        CustomWaits.checkPageReady();
        WebElement auditLogBtn = CustomWaits.extComponentQuery("panel button[reference=\"erAuditLogBtn\"]");
        ClickControl.waitAndClick(auditLogBtn);
    }

    public List<String> expandAuditLogAndGetEvents() {
        expandAuditLog();
        return getAuditEventText();
    }

    public List<String> getAuditEventText() {
        CustomWaits.waitForElement(auditLogEvents.get(0));
        return auditLogEvents.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public boolean isAuditLogVisibleFor(String eventText, String user) {
        CustomWaits.waitForElement(auditLogEvents.get(0));
        List<WebElement> ele = webDriver.findElements(By.xpath("//div[contains(@data-viewid,'audit')]"));
        WebElement eventInAudit = ele.stream().filter(element -> element.getText().contains(eventText)).findFirst().orElse(null);
        WebElement userInAudit = ele.stream().filter(element -> element.getText().contains(user)).findFirst().orElse(null);
        assert eventInAudit != null;
        assert userInAudit != null;
        return eventInAudit.isDisplayed() && userInAudit.isDisplayed();
    }

    public void expandVersionsMenu() {
        WebElement versionsMenuBtn = CustomWaits.extComponentQuery("panel button[reference=\"erVersionsBtn\"]");
        ClickControl.waitAndClick(versionsMenuBtn);
    }

    public String getVersionInformation(int version, String detail) {
        expandVersionsMenu();
        CustomWaits.isElementDisplayed(recordVersions.get(0));
        By xpath = By.xpath("//*[contains(@id,'ext-electronicrecordversiondetail')]//div[contains(@class,'textfield') and contains(.,'" + detail + "')]//input");
        List<WebElement> elements = webDriver.findElements(xpath);
        return JavaScriptControl.jsGetValue(elements.get(elements.size() - version));
    }

    public void goToVersion(int version) {
        expandVersionsMenu();
        CustomWaits.isElementDisplayed(recordVersions.get(0));
        ClickControl.waitAndClick(recordVersions.get(recordVersions.size() - version));
    }

    public void clickEdit() {
        CustomWaits.checkPageReady();
        WebElement editPencilComp = CustomWaits.extComponentQuery("button[reference=\"subRecordRerunButton\"]");
        ClickControl.waitAndClick(editPencilComp);
    }


    public boolean verifyDateFormat(String createdText) {
        CustomWaits.checkPageReady();
        String subStringDateTime = createdText.substring(0, 17);
        log.info("DateTime is: " + subStringDateTime);
        String subStringTimezone = createdText.substring(18, 23);
        log.info("Timezone is: " + subStringTimezone);
        Assert.assertTrue(DateFormatter.isValidFormat("dd MMM uuuu HH:mm", subStringDateTime));
        assert subStringTimezone.equals("+0100") || subStringTimezone.equals("+0000");
        return true;
    }

    public String getVersionDate() {
        CustomWaits.checkPageReady();
        CustomWaits.isElementDisplayed(versionGrid);
        WebElement element = webDriver.findElement(By.xpath("//*[contains(@id,'ext-electronicrecordversiondetail')]/div/div/div/div/div/div[4]//input"));
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        Object date = js.executeScript("return arguments[0].value", element);
        log.info("version date is : " + date);
        return date.toString();
    }

    public boolean warningMessageForOlderVersion() {
        CustomWaits.checkPageReady();
        return webDriver.getPageSource().toLowerCase().contains("WARNING...".toLowerCase()) && webDriver.getPageSource().toLowerCase().contains(" This record is out of date. A new version exists.".toLowerCase());
    }

    public boolean checkForVersionWarningMessage() {
        CustomWaits.checkPageReady();
        return warningMessage.isDisplayed() && messageWarningBanner.isDisplayed();
    }

    public boolean isEventLoggedBySystemUser(String user) {
        CustomWaits.checkPageReady();
        return webDriver.getPageSource().toLowerCase().contains(user.toLowerCase());
    }

    public boolean isEditButtonDisabled() {
        CustomWaits.checkPageReady();
        return CustomWaits.isElementDisplayed(editButton);
    }

}
