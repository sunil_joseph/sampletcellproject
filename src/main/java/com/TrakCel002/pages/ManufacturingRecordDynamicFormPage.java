package com.TrakCel002.pages;

import com.TrakCel002.WebControls.ClickControl;
import com.TrakCel002.WebControls.SendControl;
import com.TrakCel002.utilities.fileUtils.DIFactory;
import com.TrakCel002.waits.CustomWaits;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */

public class ManufacturingRecordDynamicFormPage extends BaseDynamicFormPage {

    @FindBy(name = "LOT_NUMBER")
    private WebElement patientLotNumber;

    @FindBy(name = "comments")
    private WebElement comments;

    @FindBy(xpath = "//*[contains(@data-viewid,'boundlist') and contains(.,'Manufacturing Site 1')]")
    private WebElement selectFacilityInDropdown;

    public ManufacturingRecordDynamicFormPage(WebDriver driver) {
        super(driver);
    }

    public RecordsPage completeLotNumberForm() {
        enterPatientLotNumber();
        extUtils.getFieldWithLabelLike("Manufacturing Facility").click();
        ClickControl.waitAndClick(selectFacilityInDropdown);
        SendControl.enterText(comments, "Will this help?");
        clickProceed();
        return getRecordsPage();
    }

    public ManufacturingRecordDynamicFormPage completeFirstLotNumberForm() {
        enterPatientLotNumber();
        extUtils.getFieldWithLabelLike("Manufacturing Facility").click();
        ClickControl.waitAndClick(selectFacilityInDropdown);
        SendControl.enterText(comments, "Will this help?");
        clickProceed();
        return getManufacturingRecordDynamicFormPage();
    }

    public ManufacturingRecordDynamicFormPage enterPatientLotNumber() {
        SendControl.enterText(patientLotNumber, DIFactory.generatePatientLotNumber() + Keys.TAB);
        return getManufacturingRecordDynamicFormPage();
    }

    public RecordsPage confirmManufacturingTask() {
        clickConfirm();
        CustomWaits.checkPageReady();
        webDriver.navigate().refresh();
        CustomWaits.waitForNoOfSeconds(2);
        return getRecordsPage();
    }

    public RecordsPage completeManufacturingRecord() {
        completeLotNumberForm();
        confirmManufacturingTask();
        return getRecordsPage();
    }


    public ManufacturingRecordDynamicFormPage clickOnCancel() {
        CustomWaits.checkPageReady();
        clickCancel();
        return getManufacturingRecordDynamicFormPage();
    }

    public ManufacturingRecordDynamicFormPage clickOnCancelPopup() {
        clickPopupCancel();
        return getManufacturingRecordDynamicFormPage();
    }

}
