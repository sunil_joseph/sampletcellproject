package com.TrakCel002.pages;
import com.TrakCel002.WebControls.ClickControl;
import com.TrakCel002.browserUtils.WebDriverFactory;
import com.TrakCel002.waits.CustomWaits;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BaseDynamicFormPage extends BasePage {



    public BaseDynamicFormPage(WebDriver driver) {
        super(driver);
    }

        private static JavascriptExecutor js = (JavascriptExecutor) WebDriverFactory.getDriver();

        @FindBy(xpath = "//*[contains(@data-componentid,'dynamicform')]//*[contains(@class,'header-title')]//*[@class='x-innerhtml']")
        private WebElement recordName;

        @FindBy(xpath = "//div[contains(@class,'dynamic-form-title')]//div[contains(@class,'x-innerhtml')]")
        private WebElement taskName;

        @FindBy(xpath = "//*[contains(@class,'x-button') and .//div[contains(., 'UPLOAD')]]")
        public WebElement uploadBtn;

        @FindBy(xpath = "//div[contains(text(),'Invalid field')]")
        private WebElement invalidErrorElement;

        @FindBy(xpath = "//*[contains(@class,'x-button') and .//div[contains(.,'SAVE')]]")
        private WebElement saveBtn;

        public String getRecordName () {
        return CustomWaits.waitForElement(recordName).getText();
    }

        public String getTaskName () {
        return CustomWaits.waitForElement(taskName).getText();
    }

        public void waitForRecordNameToBe (String expectedRecordName){
        CustomWaits.waitForTextInElement(recordName, expectedRecordName);
    }

        public void waitForTaskNameToBe (String expectedTaskName){
        CustomWaits.waitForTextInElement(taskName, expectedTaskName);
    }

        public void clickCancel () {
        CustomWaits.checkPageReady();
        WebElement dynamicFormCancelButton = CustomWaits.extComponentQuery("button[reference=\"dynamic-form-cancel-button\"]");
        ClickControl.jsWaitAndClick(dynamicFormCancelButton);
    }

        public void clickSave () {
        CustomWaits.checkPageReady();
        WebElement dynamicFormSaveButton = CustomWaits.extComponentQuery("button[reference=\"dynamic-form-save-button\"]");
        ClickControl.jsWaitAndClick(dynamicFormSaveButton);
    }

        public void clickProceed () {
        CustomWaits.checkPageReady();
        WebElement dynamicFormProceedButton = CustomWaits.extComponentQuery("button[reference=\"dynamic-form-proceed-button\"]");
        CustomWaits.waitUntilClickable(dynamicFormProceedButton);
        ClickControl.jsWaitAndClick(dynamicFormProceedButton);
    }

        public void clickConfirm () {
        CustomWaits.checkPageReady();
        extUtils.buttonWithTextLike("CONFIRM").click();
    }

        public void clickPopupCancel () {
        WebElement dynamicFormCancelButton = CustomWaits.extComponentQuery("button[text=\"CANCEL\"]");
        ClickControl.waitAndClick(dynamicFormCancelButton);
    }

        public boolean invalidFieldError () {
        CustomWaits.checkPageReady();
        return CustomWaits.isElementDisplayed(invalidErrorElement);
    }



    }

