package com.TrakCel002.pages;

import com.TrakCel002.WebControls.ClickControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */


public class ContinueTreatmentRecordDetailPage extends BaseRecordDetailPage {


    @FindBy(xpath = "//*[text()='Start']")
    private WebElement startTaskButton;

    @FindBy(xpath = "//div[contains(@data-componentid,'electronicrecorddetails')] //*[contains(text(),'Back to Treatment')]")
    private WebElement backToTreatment;

    public ContinueTreatmentRecordDetailPage(WebDriver driver) {
        super(driver);
    }

    public void startTaskButton() {
        ClickControl.waitAndClick(startTaskButton);
    }


    public RecordsPage backToTreatment() {
        ClickControl.waitAndClick(backToTreatment);
        return PageFactory.initElements(webDriver, RecordsPage.class);
    }

}
