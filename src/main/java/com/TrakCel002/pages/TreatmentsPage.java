package com.TrakCel002.pages;

import com.TrakCel002.WebControls.ClickControl;
import com.TrakCel002.utilities.fileUtils.DIFactory;
import com.TrakCel002.waits.CustomWaits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.apache.log4j.Logger;

    public class TreatmentsPage extends BasePage {

        private static final Logger log = Logger.getLogger("TestLogger");

        @FindBy(xpath = "//div[contains(@class,'add-treatment')]")
        private WebElement addTreatmentBtn;

        @FindBy(xpath = "//*[@class='x-icon-el x-font-icon mdi mdi-dots-vertical']")
        private WebElement actionButton;

        @FindBy(xpath = "//a[@data-componentid='treatmentActionMenuStop']")
        private WebElement stopTreatmentButton;

        @FindBy(name = "stopComment")
        private WebElement reasonForStopTreatment;

        @FindBy(id = "confirmStopTreatmentButton")
        private WebElement stopTreatmentConfirmButton;

        @FindBy(xpath = "//*[text()='Stopped']")
        private WebElement stoppedTreatment;

        @FindBy(xpath = "//div[contains(@class,'x-selectfield x-pickerfield x-textfield x-field x-component x-body-align-stretch')]")
        private WebElement statusFilter;

        @FindBy(xpath = "//*[contains(@class,'x-selectfield')]//*[contains(@class,'x-trigger')]")
        private WebElement allProductFilterDropdownElement;

        @FindBy(xpath = "//div[contains(@class,'x-pickerfield')][2]")
        private WebElement productsFilter;

        @FindBy(xpath = "//div[@class='treatment-tracker__list__product__head'] //*[contains(text(),'TWA Product One')]")
        private WebElement twaProductOneFilter;

        @FindBy(xpath = "//div[@class='treatment-tracker__list__product__head'] //*[contains(text(),'TWA Product Two')]")
        private WebElement twaProductTwoFilter;

        @FindBy(xpath = "//*[contains(@data-recordindex,'0') and .//*[contains(.,'No identifier available') and .//*[contains(.,'In Progress')]]]")
        private WebElement clickFirstTreatmentElement;

        @FindBy(xpath = "//*[contains(@class,'x-listitem') and .//*[contains(.,'In Progress')]]//div[contains(@class,'action')]")
        private WebElement resumedTreatment;

        @FindBy(xpath = "//*[text()='No Treatments found']")
        private WebElement noTreatmentsFound;

        @FindBy(xpath = "//*[contains(text(),'Lot Number')]")
        private WebElement lotNumberElement;

        @FindBy(xpath = "//div[contains(@class,'view-filter-reset')]")
        private WebElement resetFilterBtn;

        @FindBy(xpath = "//input[@name='PATIENT_CODE']")
        private WebElement elementPatientCode;

        @FindBy(xpath = "//input[@name='name']")
        private WebElement elementPatientName;

        @FindBy(xpath = "//*//*[@data-recordindex='0' and .//*[contains(text(),'No identifier available')]]//*[contains(text(),'Patient Name')]")
        private WebElement patientNameTagElement;

        @FindBy(xpath = "//*//*[@data-recordindex='0' and .//*[contains(text(),'No identifier available')]]//*[contains(text(),'Date of Birth')]")
        private WebElement dobtagelement;

        @FindBy(xpath = "//*//*[@data-recordindex='0' and .//*[contains(text(),'No identifier available')]]//*[contains(text(),'Last Birthday')]")
        private WebElement lastBirthdayTagElement;

        @FindBy(xpath = "//div[@class='treatment-tracker__list__product__head'] //*[contains(text(),'TWA Product One')]")
        private WebElement productOneFilter;

        public TreatmentsPage(WebDriver driver) {
            super(driver);
        }


        public boolean patientNameElementExists() {
            String xpath = "//*//*[contains(@class,'dirty x-grid-card-row-status awaiting') " +
                    "and .//*[contains(text(),'" + DIFactory.generatePatientCode() + "')]]//*[contains(text()" +
                    ",'Patient Name')]";
            return CustomWaits.waitForBySafe(By.xpath(xpath));
        }
        public boolean dateOfBirthExists() {
            String xpath = "//*//*[contains(@class,'x-listitem x-gridrow" +
                    " x-component x-paint-monitored') and " +
                    ".//*[contains(text(),'" + DIFactory.generatePatientCode() + "')]]//*[contains(text()" +
                    ",'Date of Birth')]";
            return CustomWaits.waitForBySafe(By.xpath(xpath));
        }
        public boolean lastBirthdayExists() {
            String xpath = "//*//*[contains(@class,'x-listitem x-gridrow x-component x-paint-monitored ')" +
                    " and .//*[contains(text(),'" + DIFactory.generatePatientCode() + "')] ]" +
                    "//*[contains(text(),'Last Birthday')]";
            return CustomWaits.waitForPresenceOfBy(By.xpath(xpath));
        }
        public boolean productTypeElementExists() {
            return webDriver.findElement(By.xpath("//*//*[contains(@class,'x-listitem x-gridrow x-component x-paint-monitored ') " +
                    "and .//*[contains(text(),'" + DIFactory.generatePatientCode() + "')]]//*[contains(text(),'TWA')]")).isDisplayed();
        }

        public boolean manufacturingSiteElementExists() {
            return webDriver.findElement(By.xpath("//*//*[contains(@class,'x-listitem x-gridrow x-component x-paint-monitored ')" +
                    " and .//*[contains(text(),'" + DIFactory.generatePatientCode() + "')]]//*[contains(text(),'Man')]")).isDisplayed();
        }
        public TreatmentsPage addTreatmentWithCompletedPatientReg() {
            CustomWaits.checkPageReady();
            clickAddTreatment()
                    .selectProduct("TWA Product One")
                    .completePatientRegistration()
                    .confirmPatientRegistration();
            return getTreatmentsPage();
        }
        public ProductModalPage clickAddTreatment() {
            CustomWaits.checkPageReady();
            WebElement addTreatmentButton = CustomWaits.extComponentQuery("button#addTreatmentButton");
            ClickControl.waitAndClick(addTreatmentButton);
            webDriver.switchTo().activeElement();
            return getProductPage();
        }

}
