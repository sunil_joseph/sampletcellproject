package com.TrakCel002.WebControls;

import com.TrakCel002.waits.CustomWaits;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */

public class CheckBoxControl extends BaseControl {

    private static Logger log = LogManager.getLogger("TestLogger");

    public static void tickCheckBox(WebElement element) {
        try {
            if (!isSelected(element)) {
                element.click();
                log.info("Element is clicked");
                return;
            }
            log.warn("Unable to click on the element");
        } catch (Exception e) {
            CustomWaits.waitForNoOfSeconds(1);
            return;
        }
    }

    public static void unTickCheckBox(WebElement element) {
        if (isSelected(element)) {
            element.click();
            log.info("Element is clicked");
            return;
        }
        log.warn("Unable to click on the element");
        return;
    }


    public static String getCheckBoxText(WebElement element) {
        for (int count = 0; count <= time; count++) {
            if (!isDisplayed(element) && !isEnabled(element)) continue;
            String text = element.getText();
            log.info("Element text is " + text);
            return text;
        }
        log.warn("Unable to get text on the element");
        return "";
    }
}


