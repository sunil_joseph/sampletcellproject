package com.TrakCel002.WebControls;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;

/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */
public class DateFormatter extends BaseControl {

    private static Logger log = LogManager.getLogger("TestLogger");

    public static boolean isValidFormat(String format, String value) {
        LocalDateTime ldt;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format, Locale.ENGLISH);
        log.debug("value = " + value);
        try {
            ldt = LocalDateTime.parse(value, formatter);
            String result = ldt.format(formatter);
            log.info("LocalDateTime result = " + result);
            return result.equals(value);
        } catch (DateTimeParseException e) {
            log.info(e);
            try {
                LocalDate ld = LocalDate.parse(value, formatter);
                String result = ld.format(formatter);
                log.info("LocalDate result = " + result);
                return result.equals(value);
            } catch (DateTimeParseException exp) {
                log.info(exp);
                try {
                    LocalTime lt = LocalTime.parse(value, formatter);
                    String result = lt.format(formatter);
                    log.info("LocalTime result = " + result);
                    return result.equals(value);
                } catch (DateTimeParseException e2) {
                    log.info(e2);
                }
            }
        }

        return false;
    }

    public static String formatRelativeDate(String format, int relativeDate) {
        LocalDateTime date = LocalDateTime.now().plusDays(relativeDate);
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(format);
        return date.format(dateFormat);
    }

}
