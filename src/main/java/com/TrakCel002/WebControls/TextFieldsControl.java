package com.TrakCel002.WebControls;

import com.TrakCel002.waits.CustomWaits;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */

public class TextFieldsControl extends BaseControl {

    private static Logger log = Logger.getLogger("TestLogger");

    public static void inputText(WebElement element, String requiredText) {
        for (int counter = 0; counter <= time; counter++) {
            try {
                if (isDisplayed(element) && isEnabled(element)) {
                    element.clear();
                    element.sendKeys(requiredText);
                    log.info("Has entered " + requiredText);
                    return;
                } else {
                    CustomWaits.waitForNoOfSeconds(2);
                }
                log.trace("Unable to enterText text " + requiredText);
                return;
            } catch (Exception e) {
                CustomWaits.waitForNoOfSeconds(1);
                return;
            }
        }
    }
}
