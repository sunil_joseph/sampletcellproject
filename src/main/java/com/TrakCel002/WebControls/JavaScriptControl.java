package com.TrakCel002.WebControls;

import com.TrakCel002.browserUtils.WebDriverFactory;
import com.TrakCel002.waits.CustomWaits;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class JavaScriptControl extends BaseControl {

    private static Logger log = LogManager.getLogger("TestLogger");
    private static WebDriver driver = WebDriverFactory.getDriver();
    private static JavascriptExecutor js = (JavascriptExecutor) driver;

    public static String jsGetValue(WebElement element) {
        CustomWaits.waitForElement(element);
        return js.executeScript("return arguments[0].value", element).toString();
    }

    public static String getJavaScriptAlertText() {
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();
        log.info("Javascript text is " + text);
        alert.dismiss();
        return text;
    }

    public static void confirmJavaScriptAlert() {
        Alert alert = driver.switchTo().alert();
        alert.accept();
        log.info("Javascript alert accepted");
    }

    public static void dismissJavaScriptAlert() {
        Alert alert = driver.switchTo().alert();
        alert.dismiss();
        log.info("Javascript alert dismissed");
    }

    public static Object runJsScript(String script) {
        return js.executeScript(script);
    }

    public static void scrollElementIntoView(WebElement element) {
        js.executeScript("arguments[0].scrollIntoView();", element);
        CustomWaits.isElementDisplayed(element);
    }

    public static void scrollPageToBottom() {
        js.executeScript("window.scrollBy(200,300)");
    }

    public static void scrollPageToTop() {
        js.executeScript("scroll(0, -250);");
    }
}

