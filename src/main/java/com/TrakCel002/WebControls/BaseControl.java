package com.TrakCel002.WebControls;

import com.TrakCel002.utilities.fileUtils.Props;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;



public class BaseControl {

    private static Logger log = Logger.getLogger("TestLogger");
    static int time = Integer.parseInt(Props.getValue("global.time.out"));

    static boolean isDisplayed(WebElement element) {
        for (int i = 0; i < 30; i++) {
            if (element.isDisplayed()) {
                log.info("Element is Displayed");
                return true;
            } else {
                waitForSeconds(1);
            }
        }
        return false;
    }

    public static boolean isSelected(WebElement element) {
        for (int i = 0; i < 30; i++) {
            if (element.isSelected()) {
                log.info("Element is Seleceted");
                return true;
            } else {
                waitForSeconds(2);
            }
        }
        return false;
    }

    static boolean isEnabled(WebElement element) {
        for (int i = 0; i < 30; i++) {
            if (element.isEnabled()) {
                log.info("Element is Enabled");
                return true;
            } else {
                waitForSeconds(2);
            }
        }
        return false;
    }

    private static void waitForSeconds(long seconds) {
        try {
            Thread.sleep(1000 * seconds);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }
}
