package com.TrakCel002.WebControls;

import com.TrakCel002.waits.CustomWaits;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */

public class DatePicker extends BaseControl {

    @FindBy(xpath = "//*[contains(@class,'x-datepanel x-panel') and not(contains(@class,'x-hidden'))]//*[contains(@class,'x-today')]")
    private static WebElement xToday;

    private static String getThisMonthYear() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("MMMM yyyy"));
    }

    private static String getPreviousMonthYear() {
        return LocalDateTime.now().minusMonths(1).format(DateTimeFormatter.ofPattern("MMMM yyyy"));
    }

    public static void datePicker(WebDriver driver, int relativeDate) {
        CustomWaits.checkPageReady();
        if (relativeDate == 0) {
            ClickControl.waitAndClick(driver.findElement(By.xpath("//*[contains(@class,'x-datepanel x-panel') and not(contains(@class,'x-hidden'))]//*[contains(@class,'x-today')]")));
        } else {
            List<WebElement> calendarDays = driver.findElements(By.xpath("//*[contains(@class,'x-datepanel x-panel') and not(contains(@class,'x-hidden'))]/div[contains(.,'" + getThisMonthYear() + "')]/div/div/div[2]/div[2]//td[not(contains(@class,'x-empty'))]"));
            WebElement today = driver.findElement(By.xpath("//*[contains(@class,'x-datepanel x-panel') and not(contains(@class,'x-hidden'))]//*[contains(@class,'x-today')]"));
            int indexOfToday = calendarDays.indexOf(today);
            int indexOfRelativeDate = indexOfToday + relativeDate;
            int indexOfLastDayOfMonth = calendarDays.size() - 1;
            int indexOfNextMonth;
            int indexOfPreviousMonth;
            if (indexOfRelativeDate > indexOfLastDayOfMonth) {
                indexOfNextMonth = indexOfRelativeDate - indexOfLastDayOfMonth - 1;
                WebElement nextMonth = driver.findElement(By.xpath("//*[contains(@class,'datepanel x-panel') and not(contains(@class,'x-hidden'))]//*[contains(@class,'x-right-year-tool')]"));
                ClickControl.waitAndClick(nextMonth);
                CustomWaits.checkPageReady();
                List<WebElement> nextMonthCalendarDays = driver.findElements(By.xpath("//*[contains(@class,'datepanel x-panel') and not(contains(@class,'x-hidden'))]//*[@class='x-layout-carousel x-layout-carousel-singular']/div[3]//td[not(contains(@class,'x-empty'))]"));
                ClickControl.waitAndClick(nextMonthCalendarDays.get(indexOfNextMonth));
            } else if (indexOfRelativeDate < 0) {
                By previousMonth = By.xpath("//*[contains(@class,'datepanel x-panel') and not(contains(@class,'x-hidden'))]//*[contains(@class,'x-left-year-tool')]");
                ClickControl.waitAndClick(previousMonth);
                CustomWaits.checkPageReady();
                List<WebElement> previousMonthCalendarDays = driver.findElements(By.xpath("//div/table[.//caption[contains(.,'" + getPreviousMonthYear() + "')]]//td[not(contains(@class,'x-empty'))]"));
                indexOfPreviousMonth = previousMonthCalendarDays.size() + indexOfRelativeDate;
                ClickControl.waitAndClick(previousMonthCalendarDays.get(indexOfPreviousMonth));
            } else {
                ClickControl.waitAndClick(calendarDays.get(indexOfRelativeDate));
            }
        }
        WebElement okBtn = driver.findElement(By.xpath("//*[contains(@class,'datepanel x-panel') and not(contains(@class,'x-hidden'))]//*[contains(@class,'ok-button')]//button"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", okBtn);
    }

}
