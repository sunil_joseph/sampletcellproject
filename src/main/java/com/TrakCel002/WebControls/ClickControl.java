package com.TrakCel002.WebControls;

import com.TrakCel002.browserUtils.WebDriverFactory;
import com.TrakCel002.waits.CustomWaits;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class ClickControl extends BaseControl {

    private static final WebDriver driver = WebDriverFactory.getDriver();
    private static int maxTimeToWait = 30;
    private static final Logger log = Logger.getLogger("TestLogger");
    private static final String ELEMENT_CLICKED = "Element clicked";
    private static final String JAVASCRIPT_SCROLL = "arguments[0].scrollIntoView();";
    private static final String JAVASCRIPT_CLICK = "arguments[0].click();";

    public static void waitAndClick(WebElement element) {
        CustomWaits.checkPageReady();
        CustomWaits.waitForElement(element);
        CustomWaits.checkPageReady();
        try {
            element.click();
            log.info(ELEMENT_CLICKED);
        } catch (Exception e) {
            try {
                log.error("selenium click failed to click: " + element + ", trying actions click");
                Actions actions = new Actions(driver);
                actions.moveToElement(element).click().build().perform();
                log.info(ELEMENT_CLICKED);
            } catch (Exception except) {
                log.error("actions click failed to click: " + element + ", trying javascript click");
                JavascriptExecutor js = (JavascriptExecutor) driver;
                js.executeScript(JAVASCRIPT_SCROLL, element);
                js.executeScript(JAVASCRIPT_CLICK, element);
            }
        }
    }

    public static void waitAndClick(By by) {
        CustomWaits.waitForBySafe(by);
        try {
            WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
            wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(by)));
            driver.findElement(by).click();
            log.info(ELEMENT_CLICKED);
        } catch (Exception e) {
            try {
                log.error("selenium click failed to click: " + by + ", trying actions click");
                Actions actions = new Actions(driver);
                actions.moveToElement(driver.findElement(by)).click().build().perform();
                log.info(ELEMENT_CLICKED);
            } catch (Exception except) {
                log.error("actions click failed to click: " + by + ", trying javascript click");
                JavascriptExecutor js = (JavascriptExecutor) driver;
                js.executeScript(JAVASCRIPT_SCROLL, driver.findElement(by));
                js.executeScript(JAVASCRIPT_CLICK, driver.findElement(by));
            }
        }
    }

    public static void jsWaitAndClick(WebElement element) {
        CustomWaits.isElementDisplayed(element);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(JAVASCRIPT_SCROLL, element);
        js.executeScript(JAVASCRIPT_CLICK, element);
    }

    public static void clickAndClear(WebDriver driver, WebElement element) {
        CustomWaits.isElementDisplayed(element);
        if (isEnabled(element) && isDisplayed(element)) {
            WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            Actions actions = new Actions(driver);
            actions.moveToElement(element).click().perform();
            element.clear();
            log.info("Element clicked & cleared");
        } else {
            log.info("Element is not ready to be clicked");
        }
    }

    public static void doubleClick(WebDriver driver, WebElement element) {
        CustomWaits.isElementDisplayed(element);
        WebDriverWait wait = new WebDriverWait(driver, maxTimeToWait);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        Actions actions = new Actions(driver);
        actions.doubleClick(element).perform();
        log.info("Element double clicked");
    }
}

