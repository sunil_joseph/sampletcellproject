package com.TrakCel002.WebControls;

import com.TrakCel002.browserUtils.WebDriverFactory;
import com.TrakCel002.waits.CustomWaits;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.apache.log4j.Logger;


public class SendControl {



    private static Logger log = Logger.getLogger("TestLogger");
    private static WebDriver driver = WebDriverFactory.getDriver();

    @FindBy(xpath = "//div[@class='x-list-outer-ct x-scroller']")
    private static WebElement draggablePartOfScrollbar;

    @FindBy(xpath = "//div[@class='x-body-el x-container-body-el x-component-body-el x-scroller x-layout-auto']")
    private static WebElement draggablePartOfScrollbarTwo;

    @FindBy(xpath = "//div[@class='x-body-el x-container-body-el x-component-body-el x-scroller x-layout-box x-layout-vbox x-vertical x-align-stretch x-pack-start']")
    private static WebElement draggablePartOfScrollbarThree;

    public static void enterText(WebElement element, String message) {
        CustomWaits.checkPageReady();
        if (CustomWaits.isElementDisplayed(element)) {
            if (element.getAttribute("value").isEmpty()) {
                element.sendKeys(message);
                log.info(message + "entered successfully");
            } else {
                element.clear();
                element.sendKeys(message);
                log.info(message + "entered successfully");
            }
        } else {
            log.info("Element is not ready to be interacted with");
        }
    }

    private static void resize() {
        CustomWaits.checkPageReady();
        log.trace(driver.manage().window().getSize());
        Dimension dimension = new Dimension(1600, 1200);
        driver.manage().window().setSize(dimension);

    }


    public static boolean resizeAndCheckScrollbar() {
        resize();
        CustomWaits.checkPageReady();
        if (draggablePartOfScrollbar != null) {
            return scrollLoop(draggablePartOfScrollbar);
        } else if (draggablePartOfScrollbarTwo != null) {
            return scrollLoop(draggablePartOfScrollbarTwo);
        }
        return scrollLoop(draggablePartOfScrollbarThree);
    }


    public static boolean scrollLoop(WebElement webElement) {
        Actions dragged = new Actions(driver);
        int numberOfPixelsToDragTheScrollbarDown = 50;
        for (int i = 10; i < 500; i = i + numberOfPixelsToDragTheScrollbarDown) {
            try {
                dragged.moveToElement(webElement).clickAndHold().moveByOffset(0, numberOfPixelsToDragTheScrollbarDown).release().perform();
                Thread.sleep(1000L);
            } catch (Exception e1) {
                log.debug(e1);
            }
        }
        return true;
    }
}
