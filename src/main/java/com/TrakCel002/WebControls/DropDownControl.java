package com.TrakCel002.WebControls;

import com.TrakCel002.waits.CustomWaits;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */

public class DropDownControl extends BaseControl {

    private static Logger log = LogManager.getLogger("TestLogger");
    private static By twaDropdownElement = By.xpath(
            "//*[@class='x-list-inner-ct x-show-selection x-size-monitored x-paint-monitored x-no-row-lines x-layout-auto']");
    private static By allProductFilter = By.xpath("//input[@class='x-input-el']");
    private static By allStatusFilter = By.xpath("//input[@class='x-input-el']");

    public static void selectDropDownByVisibleText(WebElement element, String visibleText) {
        for (int counter = 0; counter <= time; counter++) {
            try {
                if (isDisplayed(element) && isEnabled(element)) {
                    Select select = new Select(element);
                    select.selectByVisibleText(visibleText);
                    log.info("Element is selected is " + visibleText);
                    return;
                } else {
                    CustomWaits.waitForNoOfSeconds(4);
                    log.info("Wait is applied " + counter);
                }
            } catch (Exception e) {
                CustomWaits.waitForNoOfSeconds(3);
            }
        }
    }

    public static void selectByVisibleText(WebElement element, String text) {
        Select select = new Select(element);
        select.selectByVisibleText(text);
        CustomWaits.waitForNoOfSeconds(5);
    }

    public static void selectTwaDropDown(WebDriver driver, String text) {
        WebDriverWait wait = new WebDriverWait(driver, 25);
        wait.until(ExpectedConditions.elementToBeClickable(twaDropdownElement));
        WebElement products = driver.findElement(twaDropdownElement);
        for (int i = 0; i < products.findElements(By.tagName("span")).size(); i++) {
            if (products.findElements(By.tagName("span")).get(i).getText().contains(text)) {
                products.findElements(By.tagName("span")).get(i).click();
                break;
            }
        }
    }

    public static void selectAllProductTwaDropDownOnTaskPage(WebDriver driver, String text) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(allProductFilter));
        WebElement productsFilter = driver.findElements(allProductFilter).get(2);
        for (int i = 0; i < productsFilter.findElements(By.tagName("span")).size(); i++) {
            if (productsFilter.findElements(By.tagName("span")).get(i).getText().contains(text)) {
                productsFilter.findElements(By.tagName("span")).get(i).submit();
                break;
            }
        }
    }

    public static void selectAllStatusDropDownOnTaskPage(WebDriver driver, String text) {
        WebDriverWait wait = new WebDriverWait(driver, 25);
        wait.until(ExpectedConditions.elementToBeClickable(allStatusFilter));
        WebElement statusFilter = driver.findElements(allStatusFilter).get(1);
        for (int i = 0; i < statusFilter.findElements(By.tagName("span")).size(); i++) {
            if (statusFilter.findElements(By.tagName("span")).get(i).getText().contains(text)) {
                statusFilter.findElements(By.tagName("span")).get(i).click();
                break;
            }
        }
    }
}
