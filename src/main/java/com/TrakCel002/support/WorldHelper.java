package com.TrakCel002.support;


import com.TrakCel002.browserUtils.WebDriverFactory;
import com.TrakCel002.pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class WorldHelper {

    protected WebDriver webDriver = WebDriverFactory.getDriver();
    private static BasePage basePage;
    private static LoginPage loginPage;
    private static DashboardPage dashboardPage;
    private static PatientRegistrationRecordDynamicFormPage patientRegistrationRecordDynamicFormPage;
    private static TreatmentsPage treatmentsPage;
    private static PatientTestResultsRecordDetailPage patientTestResultsRecordDetailPage;
    private static RecordsPage recordsPage;
    private static SelectRecordTypeModal selectRecordTypeModal;
    private static ManufacturingRecordDynamicFormPage manufacturingRecordDynamicFormPage;
    private static ProductModalPage productModalPage;

    public BasePage getBasePage() {
        if (basePage != null) return basePage;
        basePage = PageFactory.initElements(webDriver, BasePage.class);
        return basePage;
    }

    public LoginPage getLoginPage() {
        if (loginPage != null) return loginPage;
        loginPage = PageFactory.initElements(webDriver, LoginPage.class);
        return loginPage;
    }

    public DashboardPage getDashboardPage() {
        if (dashboardPage != null) return dashboardPage;
        dashboardPage = PageFactory.initElements(webDriver, DashboardPage.class);
        return dashboardPage;
    }

    public TreatmentsPage getTreatmentsPage() {
        if (treatmentsPage != null) return treatmentsPage;
        treatmentsPage = PageFactory.initElements(webDriver, TreatmentsPage.class);
        return treatmentsPage;
    }

    public PatientRegistrationRecordDynamicFormPage getPatientRegistrationRecordDynamicFormPage() {
        if (patientRegistrationRecordDynamicFormPage != null) return patientRegistrationRecordDynamicFormPage;
        patientRegistrationRecordDynamicFormPage = PageFactory.initElements(webDriver, PatientRegistrationRecordDynamicFormPage.class);
        return patientRegistrationRecordDynamicFormPage;
    }

    public PatientTestResultsRecordDetailPage getPatientTestResultsRecordDetailPage() {
        if (patientTestResultsRecordDetailPage != null) return patientTestResultsRecordDetailPage;
        patientTestResultsRecordDetailPage = PageFactory.initElements(webDriver, PatientTestResultsRecordDetailPage.class);
        return patientTestResultsRecordDetailPage;
    }

    public RecordsPage getRecordsPage() {
        if (recordsPage != null) return recordsPage;
        recordsPage = PageFactory.initElements(webDriver, RecordsPage.class);
        return recordsPage;
    }


    public SelectRecordTypeModal getSelectRecordTypePage() {
        if (selectRecordTypeModal != null) return selectRecordTypeModal;
        selectRecordTypeModal = PageFactory.initElements(webDriver, SelectRecordTypeModal.class);
        return selectRecordTypeModal;
    }

    public ManufacturingRecordDynamicFormPage getManufacturingRecordDynamicFormPage() {
        if (manufacturingRecordDynamicFormPage != null) return manufacturingRecordDynamicFormPage;
        manufacturingRecordDynamicFormPage = PageFactory.initElements(webDriver, ManufacturingRecordDynamicFormPage.class);
        return manufacturingRecordDynamicFormPage;
    }

    public ProductModalPage getProductPage() {
        if (productModalPage != null) return productModalPage;
        productModalPage = PageFactory.initElements(webDriver, ProductModalPage.class);
        return productModalPage;
    }

}
