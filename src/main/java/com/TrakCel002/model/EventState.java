package com.TrakCel002.model;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/*
 * this class is based on
 * https://git.trakcel.com/scheduling/scheduling-api/blob/master/src/main/java/com/trakcel/sched/event/model/EventState.java
 * */
public enum EventState {
    NOT_BOOKED_YET("event_state.not_booked_yet", -100),
    CONFIRMED("event_state.confirmed", 0),
    APPROVED("event_state.approved", 2),
    PENDING_APPROVAL("event_state.pending_approval", 5),
    RESERVED("event_state.reserved", 10),
    RELEASED("event_state.released", 20);

    public static final EnumSet<EventState> NON_RELEASABLE_PAST_EVENT_STATES = EnumSet.of(CONFIRMED, APPROVED);

    private static final Map<String, EventState> STATE_MAP = new ConcurrentHashMap<>();

    static {
        for (final EventState state : EnumSet.allOf(EventState.class)) {
            EventState.STATE_MAP.put(state.toString(), state);
            EventState.STATE_MAP.put(state.toValue(), state);
        }
    }

    // User facing name for the event state
    private final String name;

    // User facing ordering for event states - 0 top of the list, runs down from there
    private final int order;

    EventState(String name, int order) {
        this.name = name;
        this.order = order;
    }

    public static List<String> getNames() {
        return Arrays.stream(EventState.values())
                .sorted(Comparator.comparingInt(o -> o.order))
                .map(eventState -> eventState.name)
                .collect(Collectors.toList());
    }

    public static EventState forValue(final String value) {
        return Optional.ofNullable(STATE_MAP.get(value))
                .orElseThrow(() -> new RuntimeException("Unknown value for enum EventState " + value));
    }

    public String toValue() {
        // remove content managed prefix
        String humanRedableForm = this.toString().replaceFirst("event_state.", "");
        // uppercase first letter
        // allows to convert `Confirmed` or `Reserved` to ENUM value
        humanRedableForm = humanRedableForm.substring(0, 1).toUpperCase() + humanRedableForm.substring(1);
        return humanRedableForm;
    }


    public boolean isPendingApproval() {
        return this == EventState.PENDING_APPROVAL;
    }

    public boolean isConfirmed() {
        return this == EventState.CONFIRMED;
    }

    public boolean isApproved() {
        return this == EventState.APPROVED;
    }

    public boolean isReleased() {
        return this == EventState.RELEASED;
    }

    public boolean isReserved() {
        return this == EventState.RESERVED;
    }

    public boolean hasNotBeenBooked() {
        return this == EventState.NOT_BOOKED_YET;
    }

    @Override
    public String toString() {
        return name;
    }
}
