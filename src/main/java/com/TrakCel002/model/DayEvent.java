package com.TrakCel002.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 *  This model class describe schedule day events page, which is visible after you book schedule for a given date
 */
public class DayEvent {
    private LocalDate date;
    private String eventName;
    private String resource;
    private EventState eventStatus;

    public DayEvent(LocalDate date, String eventName, String resource, EventState eventStatus) {
        this.date = date;
        this.eventName = eventName;
        this.resource = resource;
        this.eventStatus = eventStatus;
    }

    public DayEvent(LocalDate date, String eventName, String resource, String eventStatus) {
        this.date = date;
        this.eventName = eventName;
        this.resource = resource;
        this.eventStatus = EventState.forValue(eventStatus);
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public EventState getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(EventState eventStatus) {
        this.eventStatus = eventStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DayEvent dayEvent = (DayEvent) o;
        return Objects.equals(date, dayEvent.date) &&
                Objects.equals(eventName, dayEvent.eventName) &&
                Objects.equals(resource, dayEvent.resource) &&
                eventStatus == dayEvent.eventStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, eventName, resource, eventStatus);
    }

    @Override
    public String toString() {
        return "DayEvent{" +
                "date=" + date +
                ", eventName='" + eventName + '\'' +
                ", resource='" + resource + '\'' +
                ", eventStatus=" + eventStatus +
                '}';
    }
}

