package com.TrakCel002.utilities.fileUtils;

import com.TrakCel002.browserUtils.WebDriverFactory;
import com.TrakCel002.model.DayEvent;
import com.mifmif.common.regex.Generex;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;

import java.util.List;




public abstract class DIFactory extends WebDriverFactory {

    private static String generateCustomerReference;
    private static String patientCode;
    private static String patientName;
    private static String patientNameOverride;
    private static String patientLotNumber;
    private static String donorID;
    private static String donorID2;
    private static String donorName;
    private static String noOfQuantity;
    private static String noOfCopies;
    private static String clinicID;
    private static String captureEligibilityNotes;
    private static String waybillNumber;
    private static String invalidWaybillNumber;
    private static String reasonForCancellationTextMessage;
    private static String treatmentURL;
    private static String postCode;
    private static String userName;
    private static String patientInitials;
    private static String treatmentIDFromURL;
    private static String scheduleComment;
    private static String roleGroupName;

    public static List<DayEvent> getExampleScheduleProjection() {
        return exampleScheduleProjection;
    }

    public static String generateRoleGroup() {
        if (roleGroupName == null)
            roleGroupName = RandomStringUtils.randomAlphabetic(10).toLowerCase();
        return roleGroupName;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public static void setExampleScheduleProjection(List<DayEvent> exampleScheduleProjection) {
        DIFactory.exampleScheduleProjection = exampleScheduleProjection;
    }

    public static List<DayEvent> getSecondExampleScheduleProjection() {
        return secondExampleScheduleProjection;
    }

    public static void setSecondExampleScheduleProjection(List<DayEvent> secondExampleScheduleProjection) {
        DIFactory.secondExampleScheduleProjection = secondExampleScheduleProjection;
    }

    public static List<DayEvent> getApprovalScheduleProjection() {
        return approvalScheduleProjection;
    }

    public static void setApprovalScheduleProjection(List<DayEvent> approvalScheduleProjection) {
        DIFactory.approvalScheduleProjection = approvalScheduleProjection;
    }

    public static List<DayEvent> getNestedScheduleProjection() {
        return nestedScheduleProjection;
    }

    public static void setNestedScheduleProjection(List<DayEvent> nestedScheduleProjection) {
        DIFactory.nestedScheduleProjection = nestedScheduleProjection;
    }

    private static List<DayEvent> exampleScheduleProjection;
    private static List<DayEvent> secondExampleScheduleProjection;
    private static List<DayEvent> approvalScheduleProjection;
    private static List<DayEvent> nestedScheduleProjection;

    public static String generateCustomerReference() {
        if (generateCustomerReference == null)
            generateCustomerReference = RandomStringUtils.randomAlphabetic(8).toUpperCase();
        return generateCustomerReference;
    }

    public static String generatePatientCode() {
        if (patientCode == null)
            patientCode = RandomStringUtils.randomNumeric(5);
        return patientCode;
    }

    public static String patientCodeSecond() {
        patientCode = RandomStringUtils.randomNumeric(5);
        return patientCode;
    }

    public static String generatePatientNameSecond() {
        patientName = RandomStringUtils.randomAlphabetic(4).toUpperCase();
        return patientName;
    }

    public static String generatePatientCodeOldest() {
        if (patientCode == null)
            patientCode = RandomStringUtils.randomNumeric(5);
        return patientCode;
    }

    public static String generatePatientName() {
        if (patientName == null)
            patientName = RandomStringUtils.randomAlphabetic(4).toUpperCase();
        return patientName;
    }

    public static String generateEditOfPatientName() {
        if (patientNameOverride == null)
            patientNameOverride = RandomStringUtils.randomAlphabetic(6).toUpperCase();
        return patientNameOverride;
    }

    public static String generateInvalidPatientName() {
        if (patientName == null)
            patientName = RandomStringUtils.randomAlphabetic(4).toLowerCase();
        return patientName;
    }

    public static String generatePatientCodeVial() {
        if (patientCode != null)
            return patientCode + "-1";
        return generatePatientCode() + "-1";
    }

    public static String generatePatientLotNumber() {
        if (patientLotNumber == null)
            patientLotNumber = RandomStringUtils.randomNumeric(5);
        return patientLotNumber;
    }

    public static String generateDonorID() {
        if (donorID == null)
            donorID = RandomStringUtils.randomNumeric(5);
        return donorID;
    }

    public static String generateDonorIDTwo() {
        if (donorID2 == null)
            donorID2 = RandomStringUtils.randomNumeric(5);
        return donorID2;
    }

    public static String generateDonorName() {
        if (donorName == null)
            donorName = RandomStringUtils.randomAlphabetic(4).toUpperCase();
        return donorName;
    }

    public static String generateNoOfQuantity() {
        if (noOfQuantity == null)
            noOfQuantity = RandomStringUtils.randomNumeric(1);
        return noOfQuantity;
    }

    public static String generateNoOfCopies() {
        if (noOfCopies == null)
            noOfCopies = RandomStringUtils.randomNumeric(1);
        return noOfCopies;
    }

    public static String generateClinicId() {
        if (clinicID == null)
            clinicID = RandomStringUtils.randomNumeric(5);
        return clinicID;
    }

    public static String generateCaptureEligibilityNotes() {
        if (captureEligibilityNotes == null)
            captureEligibilityNotes = RandomStringUtils.randomAlphabetic(10).toLowerCase();
        return captureEligibilityNotes;
    }

    public static String generateWaybillNumber() {
        if (waybillNumber == null)
            waybillNumber = RandomStringUtils.randomNumeric(8);
        return waybillNumber;
    }

    public static String generateInvalidWaybillNumber() {
        if (invalidWaybillNumber == null)
            invalidWaybillNumber = RandomStringUtils.randomNumeric(8);
        return invalidWaybillNumber;
    }

    public static String generateReasonForCancellationTextMessage() {
        if (reasonForCancellationTextMessage == null)
            reasonForCancellationTextMessage = RandomStringUtils.randomAlphabetic(30).toLowerCase();
        return reasonForCancellationTextMessage;
    }

    public static String getTreatmentURL() {
        WebDriver driver = getDriver();
        if (treatmentURL == null)
            treatmentURL = driver.getCurrentUrl();
        return treatmentURL;
    }

    public static String generateUserName() {
        if (userName == null)
            userName = RandomStringUtils.randomAlphabetic(10).toLowerCase();
        return userName;
    }

    public static String generatePostcode() {
        Generex generex = new Generex("/^(((([A-Z][A-Z]{0,1})[0-9][A-Z0-9]{0,1}) {0,}[0-9])[A-Z]{2})$/");
        String generatedPostcode = generex.random();
        if (postCode == null)
            postCode = generatedPostcode;
        return postCode;

    }

    public static String generatedPatientInitials() {
        if (patientInitials == null)
            patientInitials = RandomStringUtils.randomAlphabetic(1).toLowerCase() + "." + RandomStringUtils.randomAlphabetic(1).toLowerCase();
        return patientInitials;
    }

    public static String treatmentIDFromURL() {
        String urlSplit = getTreatmentURL();
        String[] parts = urlSplit.split("/");
        String part5 = parts[5];
        if (treatmentIDFromURL == null)
            treatmentIDFromURL = part5;
        return treatmentIDFromURL;
    }

    public static String generateScheduleComment() {
        if (scheduleComment == null)
            scheduleComment = RandomStringUtils.randomAlphabetic(5) + RandomStringUtils.randomAlphabetic(5);
        return scheduleComment;
    }

}
