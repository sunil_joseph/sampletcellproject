package com.TrakCel002.utilities.fileUtils;

import com.TrakCel002.waits.CustomWaits;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;

import java.io.File;

public class FileDownload {

    private FileDownload() {
        throw new IllegalStateException("FileDownload class");
    }

    private static Logger log = LogManager.getLogger("TestLogger");

    public static boolean isFileDownloaded(String fileName) {
        CustomWaits.checkPageReady();
        File folder = new File(System.getProperty("user.home") + "//Downloads");

        //List the files on that folder
        File[] listOfFiles = folder.listFiles();
        boolean found = false;
        File f = null;
        //Look for the file in the files
        // You should write smart REGEX according to the filename
        assert listOfFiles != null;
        for (File listOfFile : listOfFiles) {
            if (listOfFile.isFile()) {
                String fileName1 = listOfFile.getName();
                log.debug("File " + listOfFile.getName());
                if (fileName.matches(fileName1)) {
                    f = new File(fileName);
                    found = true;
                }
            }
        }
        Assert.assertTrue("Downloaded document is not found", found);
        f.deleteOnExit();
        return true;
    }
}
