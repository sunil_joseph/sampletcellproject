package com.TrakCel002.utilities.fileUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Props {
    static String currentEnv = System.getProperty("env");

    public static String getValue(String keyName) {
        String settingFilePath = "/src/main/resources/configs/" + currentEnv + ".properties";
        String filePath = System.getProperty("user.dir") + settingFilePath;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties properties = new Properties();
        try {
            properties.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(keyName);
    }

    public static String getTData(String keyName) {
        String settingFilePath = "/src/main/resources/testdata/testdata.properties";
        String filePath = System.getProperty("user.dir") + settingFilePath;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties properties = new Properties();
        try {
            properties.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(keyName);
    }


    public static String getPassword(String username) {
        String filePath = System.getProperty("user.dir") + "/src/main/resources/testdata/users.properties";
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties properties = new Properties();
        try {
            properties.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(username);
    }
}

