package com.TrakCel002.utilities.fileUtils;

import com.TrakCel002.browserUtils.WebDriverFactory;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


/**
 * Copyright(c)2019 Trakcel Limited. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Trakcel Limited.("ConfidentialInformation").You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Trakcel Ltd.
 */
public class Screenshots {

    private static WebDriver driver = WebDriverFactory.getDriver();
    private static final Logger LOGGER = LogManager.getLogger(Screenshots.class);

    private Screenshots() {}

    public static byte[] takeShot(String screenName) {
        final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        LOGGER.info("Camera has been set and ready for taking screenshot");
        final String timeStamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HHmmSS"));
        final String systemPath = System.getProperty("user.dir");
        final String replacedScreenName = screenName.replace("\"", "'");
        final String fullPath = systemPath + getReportPath() + replacedScreenName + "_" + timeStamp;

        File destFile = new File(fullPath + ".png");
        try {
            FileUtils.copyFile(srcFile, destFile);
            LOGGER.info("Screenshot for " + screenName + " method has been taken and stored");
        } catch (IOException e) {
            LOGGER.error("Error while copying file: " + e.getMessage());
            LOGGER.error("Screenshot for " + screenName + " method has NOT been taken due to error above");
        }
        return screenshot;
    }

    private static String getReportPath() {
        String placeOfExecution = System.getProperty("env").toLowerCase();
        String reportPath = null;
        final String path = "/target/test_output/screen_report";
        if (placeOfExecution.toLowerCase().contains("local")
                || placeOfExecution.toLowerCase().contains("aws")
                || placeOfExecution.toLowerCase().contains("saucelabs")
                || placeOfExecution.equalsIgnoreCase("docker")) {
            reportPath = path;
        }
        return reportPath;
    }
}
