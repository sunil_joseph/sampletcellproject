package com.TrakCel002.Ext;

import com.TrakCel002.browserUtils.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class ExtUtils {

    private static WebDriver driver = WebDriverFactory.getDriver();

    public WebElement getFieldWithLabelLike(String fieldLabel) {
        return extComponentQuery(
                "field",
                "function(field) {return field.getLabel() == '" + fieldLabel + "';}",
                INPUT_EL_ID);
    }

    public WebElement buttonWithTextLike(String buttonText) {
        return extComponentQuery(
                "button",
                "function(field) {return field.getText() == '" + buttonText + "';}",
                EL_ID);
    }

    private final String INPUT_EL_ID = "(function (cmp) {return cmp.inputElement.id;})";
    private final String EL_ID = "(function (cmp) {return cmp.id;})";
    private final String IDENTITY = "(function (cmp) {return cmp;})";

    private WebElement extComponentQuery(String componentQueryString, String filterFunction, String resultFunction) {
        String jsFn = "" +
                "var queryArray = Ext.ComponentQuery.query('" + componentQueryString + "')" +
                "   .filter(" + filterFunction + ");" +
                "if (queryArray.length === 0) return null;" +
                "return " + resultFunction + "(queryArray[0])";

        String id = Polling.poll(() -> (String) ((JavascriptExecutor) driver).executeScript(jsFn));
        WebElement element = driver.findElement(By.id(id));
        return element;
    }

    public void clickComboItem(String comboFieldName, String item) {
        getFieldWithLabelLike(comboFieldName).click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        extComponentQuery("simplelistitem", "function(listItem) {" +
                "   return listItem.getRecord().get('display') == '" + item + "';" +
                "}", EL_ID).click();
    }

    public void clickFirstComboItem(String comboFieldName) {
        getFieldWithLabelLike(comboFieldName).click();
        extComponentQuery("simplelistitem", "function(listItem) {" +
                "   return true;" +
                "}", EL_ID).click();
    }

    public WebElement recordCard(String recordName) {
        WebElement recordRow = extComponentQuery("gridrow",
                "function(row) { " +
                        "   return row.getRecord().get('description') == '" + recordName + "';" +
                        "}",
                EL_ID
        );
        return recordRow;
    }

    public void claimTaskFromRecordDetail(String taskName) {
        String filterFunction = taskName == null ? "function() {return true;}" :
                "function(row) {return row.getRecord().get('taskDescription') === '" + taskName + "'}";

        WebElement taskRow = extComponentQuery("outstandingtasklist gridrow",
                filterFunction,
                EL_ID);

        WebElement taskButton = extComponentQuery("[id=" + taskRow.getAttribute("id") + "] widgetcell button",
                "function() {return true;}",
                EL_ID);

        taskButton.click();
    }

    public String statusOfRecordCard(String recordName) {

        WebElement recordRow = recordCard(recordName);

        return (String) ((JavascriptExecutor) driver).executeScript("return " +
                "Ext.getCmp('" + recordRow.getAttribute("id") + "')" +
                "   .getRecord()" +
                "   .get('status');"
        );

    }

    public WebElement treatmentListRow() {
        return extComponentQuery("treatmentlist", "function (row) { return row.isVisible(); }", EL_ID);
    }

    public WebElement firstInList() {
        return extComponentQuery("gridrow", "function (row) {return row.isVisible();}", EL_ID);
    }

    public WebElement checkBox(String fieldName) {
        return extComponentQuery("checkbox", "function(check) {return check.getLabel() === '" + fieldName + "'}", INPUT_EL_ID);
    }

    public void claimTaskFromPatientList(String patientId, String taskName) {
        WebElement recordRow = extComponentQuery("gridrow",
                "function(row) { " +
                        "   return row.getRecord().get('primaryIdentifierString').includes('" + patientId + "');" +
                        "}",
                EL_ID
        );

        String filterFunction = taskName == null ? "function() {return true;}" :
                "function(btn) {return btn.getText() === '" + taskName + "'}";

        WebElement taskButton = extComponentQuery("[id=" + recordRow.getAttribute("id") + "] treatmentstatuscell button",
                filterFunction,
                EL_ID);

        taskButton.click();
    }
}

