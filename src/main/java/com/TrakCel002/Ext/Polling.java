package com.TrakCel002.Ext;

import java.time.Duration;
import java.util.function.Supplier;

public class Polling {

    public static <T> T poll(Supplier<T> action) {
        return poll(action, 20, Duration.ofSeconds(20));
    }

    public static <T> T poll(Supplier<T> action, int numberOfTimes, Duration delta) {
        for (int i = 0; i < numberOfTimes; i++) {
            T result = action.get();
            if (result != null) return result;

            try {
                Thread.sleep(delta.toMillis());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        throw new RuntimeException("Failed to get non-null result");
    }
}

