package com.TrakCel002.browserUtils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverFactory {

    private static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    public static synchronized void setDriver(String browser) {
        System.out.println("BROWSER IS ------------->>>>>"+browser);
        switch (browser) {
            case "chrome":
                driver = ThreadLocal.withInitial(() -> {
                    WebDriverManager.chromedriver().setup();
                    return new ChromeDriver(BrowserOptions.getChromeOptions());
                });
                System.out.println("WEBDRIVER IN SETDRIVER FOR CHROME-----------"+driver);
                prepareBrowser();
                break;
            case "ie":
                driver = ThreadLocal.withInitial(() -> {
                    WebDriverManager.iedriver().setup();
                    return new InternetExplorerDriver(BrowserOptions.getInternetExplorerOptions());
                });
                prepareBrowser();
                break;
            case "firefox":
                driver = ThreadLocal.withInitial(() -> {
                    WebDriverManager.firefoxdriver().setup();
                    return new FirefoxDriver(BrowserOptions.getFirefoxOptions());
                });
                prepareBrowser();
                break;
        }
    }

    private static void prepareBrowser() {
        getDriver().manage().window().maximize();
        getDriver().manage().deleteAllCookies();
        getDriver().manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        getDriver().manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    public static synchronized WebDriver getDriver() {
        return driver.get();
    }

    public static void closeBrowser() {
        getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        getDriver().quit();
        driver.remove();
    }
}


